<?php
/**
 * Welcome widget content
 */

namespace Soil\Dashboard;

use Soil\Core;
use Soil\Core\Functions;
use Soil\Core\Vendor;
use Soil\Core\User;

$current_user = wp_get_current_user();
$vendor = Core::vendor();
$vendorClass = new Vendor;
$userClass = new User;

?>

<div class="welcome-panel-content">

    <h2><?php echo get_bloginfo('name'); ?> CMS </h2>
    <p class="about-description"><?php echo sprintf( __( 'Hi %1$s, let\'s manage your website:', 'joldnl-soil' ), $current_user->display_name ); ?></p>

    <div class="welcome-panel-column-container">

        <div class="welcome-panel-column">
            <div style="padding-right: 20px;">
                <h3><?php echo $vendor->supportTitle; ?></h3>
                <p>
                    <?php echo $vendor->supportIntro; ?><br>
                    <?php if ($vendor->supportUrl != $vendorClass::VENDOR_SUPPORTURL): ?>
                        <a class="button button-primary button-hero" target="_blank" href="<?php echo $vendor->supportUrl; ?>">
                            <?php _e( 'Contact', 'joldnl-soil' ); ?>
                                <?php echo $vendor->name; ?>
                            <?php _e( 'Support', 'joldnl-soil' ); ?>
                        </a>
                    <?php endif; ?>
                </p>
                <p>
                    <?php _e( 'Give us a call', 'joldnl-soil' ); ?>: <a href="tel:<?php echo $vendor->phone; ?>"><?php echo $vendor->phone; ?></a>,<br />
                    <?php _e( 'or visit the our site', 'joldnl-soil' ); ?>
                    <a href="<?php echo $vendor->websiteUrl; ?>" rel="external" target="_blank"><?php echo $vendor->getOptionSupportUrlClean(); ?></a>.
                </p>
            </div>
        </div>

        <div class="welcome-panel-column">
            <h3><?php _e( 'Userfull links:', 'joldnl-soil' ); ?></h3>
            <ul>
                <li>
                    <a href="<?php echo get_bloginfo('url'); ?>" target="_blank">
                        <i class="dashicons dashicons-admin-home" style="padding-right: 7px;"></i>
                        <?php _e( 'View your live website', 'joldnl-soil' ); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_admin_url() . 'post.php?post=' . get_option( 'page_on_front' ) . '&action=edit'; ?>">
                        <i class="dashicons dashicons-welcome-write-blog" style="padding-right: 7px;"></i>
                        <?php _e( 'Edit your front page', 'joldnl-soil' ); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo get_admin_url(); ?>post-new.php?post_type=page">
                        <i class="dashicons dashicons-plus-alt" style="padding-right: 7px;"></i>
                        <?php _e( 'Add a new page', 'joldnl-soil' ); ?>
                    </a>
                </li>
                <?php if ( ! $userClass->is_client_user() ): ?>
                    <li>
                        <a href="<?php echo get_admin_url(); ?>nav-menus.php">
                            <i class="dashicons dashicons-menu" style="padding-right: 7px;"></i>
                            <?php _e( 'Manage Menu\'s', 'joldnl-soil' ); ?>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>

        <div class="welcome-panel-column welcome-panel-last">
            <h3><?php _e( 'Other actions', 'joldnl-soil' ); ?></h3>
            <ul>
                <li><a href="<?php echo get_admin_url(); ?>options-general.php" class="welcome-icon welcome-edit-page"><?php _e( 'Change preferences', 'joldnl-soil' ); ?></a></li>
            </ul>
        </div>

    </div>
</div>
