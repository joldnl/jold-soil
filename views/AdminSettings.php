<?php
    // Check if tabs are set add set $tab variable
    $tab = 'main';
    if( isset( $_GET[ 'tab' ] ) ) {
        $tab = $_GET[ 'tab' ];
    }
?>

<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo esc_html( get_admin_page_title() ); ?></h1>

    <h2 class="nav-tab-wrapper">
        <a href="?page=soil-settings-page&tab=main" class="nav-tab <?php echo $tab == 'main' ? 'nav-tab-active' : ''; ?>">General</a>
        <a href="?page=soil-settings-page&tab=vendor" class="nav-tab <?php echo $tab == 'vendor' ? 'nav-tab-active' : ''; ?>">White Label</a>
        <a href="?page=soil-settings-page&tab=yoast" class="nav-tab <?php echo $tab == 'yoast' ? 'nav-tab-active' : ''; ?>">Yoast</a>
        <a href="?page=soil-settings-page&tab=image" class="nav-tab <?php echo $tab == 'image' ? 'nav-tab-active' : ''; ?>">Image Optimization</a>
    </h2>

    <form action="options.php" method="post">
    <?php

        if ( $tab == 'general' ) {
            settings_fields( 'soil-settings-page' );
            do_settings_sections( 'soil-settings-page' );
        } elseif ( $tab == 'vendor' ) {
            settings_fields( 'soil-settings-vendor-page' );
            do_settings_sections( 'soil-settings-vendor-page' );
        } elseif ( $tab == 'yoast' ) {
            settings_fields( 'soil-settings-yoast-page' );
            do_settings_sections( 'soil-settings-yoast-page' );
        } elseif ( $tab == 'image' ) {
            settings_fields( 'soil-settings-image-page' );
            do_settings_sections( 'soil-settings-image-page' );
        } else {
            settings_fields( 'soil-settings-page' );
            do_settings_sections( 'soil-settings-page' );
        }

        submit_button( 'Save Settings' );

    ?>
    </form>
</div>
