<?php

namespace Soil\Cli;

use Soil\Core;
use Soil\Core\Functions;
use \ShortPixel\ShortPixel;
use \WP_CLI;

/**
 * Extend Carerix synchronization to Posts
 */
class Soil {

    function __construct() {

        require_once( \Soil\Core::plugin_path() . 'vendor/shortpixel/shortpixel-php/lib/ShortPixel.php' );

        \ShortPixel\setKey( get_option('setting_image_shortpixel_key') );


        // \WP_CLI::success( 'S01L R007 C0MM6ND' );
        // \WP_CLI::success( 'Soil Root Command' );

    }



    /**
     * Get the location (dir) of the wp uploads folder
     * @return string   The dir of the uploads folder
     */
    private function getAssetsDir() {
        $assets_path = wp_upload_dir()['path'];
        return $assets_path;
    }



    /**
     * Get the cache folder contents
     * @return array    An array with all cache subfolders
     */
    private function getCacheDir() {
        $cache_dir = glob($this->getAssetsDir() . '/cache/*' , GLOB_ONLYDIR);
        return $cache_dir;
    }



    /**
     * Check if an -optimized.webp version of the file exists
     * @param  string   $dir    The directory path of the cache folder
     * @param  string   $file   The filename to look for
     * @return boolean          Returns true if a optimized webp version exists
     */
    private function getOptimizedFile( $dir = null, $file = null, $extension = null ) {

        if ( isset($dir) && $dir !== null && isset($file) && $file !== null && isset($extension) && $extension !== null  ) {

            $filename   = $this->optimizedPath($dir, $file, $extension);
            $isWepb     = (file_exists($filename) ? true : false);

            if ( $isWepb ) {

                return true;

            }

        } else {

            // Throw an exception if $string or $characters are not set or false
            \WP_CLI::error('Function checkWebP expects both $dir and $file parameters, one of them is not set.');

        }

        return false;

    }


    /**
     * Create a optimized version of the file path
     * @param  string   $dir    The directory path of the cache folder
     * @param  string   $file   The filename to look for
     * @return boolean          Returns an file path with optimized suffix
     */
    private function optimizedPath( $dir = null, $file = null, $extension = null ) {

        if ( isset($dir) && $dir !== null && isset($file) && $file !== null && isset($extension) && $extension !== null ) {

            $dir    = $dir . '/' . $file;
            $parts  = pathinfo($dir);
            $path   = $parts['dirname'] . '/' . $parts['filename'] . '-optimized.' . $extension;

            return $path;

        }

        return false;

    }




    /**
     * Display a table with all assets in cache, and check if they are optimized
     * @return string   The cli output table with assets file details
     */
    public function checkAssets() {

        // Get the wp-uploads dir path
        $assets_path = $this->getAssetsDir();

        // Get the cache folder contents
        $cache_dir   = $this->getCacheDir();

        // Create table header/footer
        $tableHeader = '';
        $tableHeader .= "+--------------+-----------------------------------------------------------------+-----------+-------+\n";
        $tableHeader .= "| Cache ID     | File                                                            | Optimized | WebP  |\n";
        $tableHeader .= "+--------------+-----------------------------------------------------------------+-----------+-------+";

        \WP_CLI::log( $tableHeader );

        $f = 0;

        foreach ($cache_dir as $dir) {

            // Explode directory path into an array
            $dirSections = explode('/', $dir);

            // Get the cache folder name
            $dirName     = end($dirSections);

            // Get all files except system and ignored files
            $files       = array_diff(scandir($dir), array('.', '..', '.svn', '.htaccess', '.DS_Store'));

            $i=0;

            // Loop trough all the files in the cache folder
            foreach ($files as $file) {

                $path_parts         = pathinfo($file);
                $fileSections       = explode('/', $file[2]);
                $statusOptimized    = \WP_CLI::colorize("%n %wFALSE%n ");
                $statusWebP         = \WP_CLI::colorize("%n %wFALSE%n ");

                // Only get original files, not the already optimized ones
                if ( !strpos($path_parts['filename'], '-optimized') ) {

                    // Optimized jpg present!
                    if ( $this->getOptimizedFile($dir, $file, $path_parts['extension']) ) {
                        $statusOptimized = \WP_CLI::colorize(" %gTRUE%n  ");
                    }

                    // Optimized WebP present!
                    if ( $this->getOptimizedFile($dir, $file, 'webp') ) {
                        $statusWebP = \WP_CLI::colorize(" %gTRUE%n  ");
                    }

                    // Write row with details to the table
                    \WP_CLI::log( '| ' . $this->fixedString($dirName, 12) . ' | ' . $this->fixedString($path_parts['filename'] . '.' . $path_parts['extension'], 60) . '    |' . $statusOptimized . '    |' . $statusWebP . '|');

                }

                $f++; $i++;

            }

        }

        // Write table footer
        \WP_CLI::log( $tableHeader );

    }




    /**
     * Loop trough all original cached (wpthumb generated) asset files and generate optimized versions (webp, jpg) trough ShortPixel API
     * @return string   The cli output table with assets file details
     */
    public function optimizeAssets() {


        /**
         * Check of 'setting_image_shortpixel_key' is set
         * LbSaVZSRKFlIM9BvoaFH
         */
        if ( empty($this->checkOption('setting_image_shortpixel_key')) || $this->checkOption('setting_image_shortpixel_key') == false) {

            \WP_CLI::error( 'Aborting: ShortPixel Key is not set!' );
            return false;

        }


        /**
         * Check of 'setting_image_enable_optimization' is enabled
         */
        if ( $this->checkOption('setting_image_enable_optimization') != true) {

            \WP_CLI::error( 'Aborting: Image optimization is disabled in Soil Settings.' );
            return false;

        }



        // Get the wp-uploads dir path
        $assets_path = $this->getAssetsDir();

        // Get the cache folder contents
        $cache_dir   = glob($assets_path . '/cache/*' , GLOB_ONLYDIR);

        $f = 0;

        foreach ($cache_dir as $dir) {

            // Write output table header
            $dirSections = explode('/', $dir);

            // Write output table header
            $dirName     = end($dirSections);

            // Write output table header
            $files       = array_diff(scandir($dir), array('.', '..', '.svn', '.htaccess', '.DS_Store'));

            $i=0;

            // Loop trough all the files in the cache folder
            foreach ($files as $file) {

                $path_parts         = pathinfo($file);
                $fileSections       = explode('/', $file[2]);
                $statusOptimized    = \WP_CLI::colorize("%n %wFALSE%n ");
                $statusWebP         = \WP_CLI::colorize("%n %wFALSE%n ");

                // Only get original files, not the already optimized ones
                if ( !strpos($path_parts['filename'], '-optimized') ) {

                    // Check if optimized files already exists, of not, start to generate new ones
                    if ( !$this->getOptimizedFile($dir, $file, $path_parts['extension']) || !$this->getOptimizedFile($dir, $file, 'webp') ) {


                        /**
                         * Check of 'setting_image_enable_compression' is enabled
                         */
                        if ( $this->checkOption('setting_image_enable_compression') == true) {

                            // Generate JPG optimized
                            if ( !$this->getOptimizedFile($dir, $file, $path_parts['extension']) ) {
                                $this->generateOptimizedFile($dir . '/' . $file, 'jpg');
                            }

                        } else {
                            \WP_CLI::error( 'Optimizing jpg images is disabled in Soil Settings. ' );
                        }


                        /**
                         * Check of 'setting_image_enable_webp' is enabled
                         */
                        if ( $this->checkOption('setting_image_enable_webp') == true) {

                            // Generate WebP file
                            if ( !$this->getOptimizedFile($dir, $file, 'webp') ) {
                                $this->generateOptimizedFile($dir . '/' . $file, 'webp');
                            }

                        } else {
                            \WP_CLI::error( 'Creating WebP images is disabled in Soil Settings. ' );
                        }


                    }

                }

                $f++;
                $i++;

            }

        }

        \WP_CLI::success( 'All images optimized!' );

    }



    /**
     * Generate an optimized .jpg file to disk cache trough ShortPixel
     * @return string   Reports back to the log with a status
     */
    private function generateOptimizedFile( $file, $extension ) {

        $fileInfo = pathinfo( $file );
        $path     = $this->optimizedPath( $fileInfo['dirname'], $fileInfo['filename'], $fileInfo['extension']);
        $pathInfo = pathinfo($path);

        if ( class_exists('\ShortPixel\ShortPixel') ) {

            if ($extension == 'webp') {
                $ret = \ShortPixel\fromFile( $file )->generateWebP()->toFiles( $pathInfo['dirname'], $pathInfo['basename'] );
            }
            if ($extension == 'jpg') {
                $ret = \ShortPixel\fromFile( $file )->toFiles( $pathInfo['dirname'] , $pathInfo['basename'] );
            }

            if ( isset( $ret->succeeded[0] ) ) {
                $succeeded = true;
            } else {
                $succeeded = false;
            }

            $this->logOptimization( $fileInfo, $extension, $succeeded );


        } else {

            // Throw an exception if $string or $characters are not set or false
            \WP_CLI::log( 'Class \ShortPixel\ShortPixel not found.' );

        }

    }



    /**
     * Output some logging details about the optimization process
     * @param  array    $path_parts     Array with the path parts
     * @param  string   $type           The image type to optimize
     * @param  boolean  $succeeded      True if the optimization is succesful
     * @return string                   Returns the log output to
     */
    private function logOptimization( $path = null, $type = null, $succeeded = null ) {

        $optimized = pathinfo( $this->optimizedPath( $path['dirname'], $path['filename'], $type) );

        Functions::log( '-- Optimize .'  . $type . ' file ----------------------------------------------------------------------' );
        Functions::log( '   File:      ' . $path['basename'] );
        Functions::log( '   Optimized: ' . $optimized['basename'] );
        //
        if ( isset($succeeded) && $succeeded == true ) {
            Functions::log( '   Status:    ' . \WP_CLI::colorize("%9%gSuccess!!%n"));
        } else {
            Functions::log( '   Status:    ' . \WP_CLI::colorize("%9%rFAILED!!%n") );
        }

        Functions::log('');

    }





    /**
     * Fix a string length, shorten if input is longer than $characters, add spaces if its shorter than the $characters
     * @param  string   $string         The input text
     * @param  int      $characters     Output string length
     * @return string                   The fixed lenght string output
     */
    private function fixedString( $string = null, $characters = null ) {

        if ( isset($string) && $string !== null && isset($characters) && $characters !== null  ) {

            // Input string length
            $stringLength = strlen($string);


            // Check if input is shorter than max length
            if ( $stringLength < $characters ) {
                $output = str_pad( $string, $characters, ' ');
            }

            // Check if input is longer than max length
            if ( $stringLength >= $characters ) {
                $output = '..' . substr($string, (- $characters + 2) );
            }

            return $output;

        }

        return false;

    }



    /**
     * Check a wordpress optino
     * @param  string   $optionKey   The key of the option from the options table
     * @return mixed                 Returns the option, or false of the option doesnt exists
     */
    private function checkOption( $optionKey = null ) {

        if ( isset($optionKey) && $optionKey !== null ) {

            $option = get_option($optionKey);

            if ( !empty($option) ) {
                return $option;
            }

        }

        return false;

    }


}



if ( defined( 'WP_CLI' ) && WP_CLI ) {

    \WP_CLI::add_command( 'soil', 'Soil\Cli\Soil' );

}
