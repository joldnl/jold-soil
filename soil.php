<?php
/**
 * Plugin Name:     Jold: Soil
 * Plugin URI:      https://bitbucket.org/joldnl/jold-soil
 * Description:     Groundwater Core WordPress Plugin.
 * Plugin Type:     mu-plugin
 *
 * Author:          Jurgen Oldenburg <info@jold.nl>
 * Author URI:      http://www.jold.nl
 *
 * Text Domain:     joldnl-soil
 * Domain Path:     /languages
 * Version:         1.2.6
 *
 * @package         soil
 *
 * Bitbucket Plugin URI: https://bitbucket.org/joldnl/jold-soil
 *
 */


require_once('bootstrap.php');
