# Soil Core WP Plugin


![Jold Soil - WordPress Core Plugin](http://assets.jold.nl/wp-framework/images/banner-soil.png)


The main Groundwater core functionality plugin containing global settings, cleanup and WordPress modifications for a clean and better user experience.
this plugin is ***mandatory*** (mu-plugin, and cannot be disabled) when using the starter theme '[Firestarter](https://bitbucket.org/joldnl/jold-firestarter)'.


## Cleanup
General cleanup functionality making WordPress and wp-admin clean and mean.

#### Admin:
*Main wp-admin cleanup functions enhancing the default wp-admin.*

- Disable the admin bar on the frontend of the site
- Remove admin menu items
- Hide some default admin dashboard widgets
- Hide edit-screen metaboxes
- Remove all default WordPress theme widgets
- Set the wp-admin footer contents to custom text and url
- Add all custom image sizes to the "Size" select box in the media pop up
- Hide page attributes metabox on edit page screens
- Hide update messages for all roles except "Administrator"

#### Frontend:
*Cleanup frontend stuff, remove head tags, and clean several frontend related things.*

- Cleanup the frontend head section.
- Add custom body classes matching with BEM class naming principles.
- Replace target="\_blank" with rel="external" for content and nav items
- Redirect search results from /?s=query to /search/query/, converts %20 to +
- Fix empty search queries and redirect them to the home page
- Remove script inclusion in wp_footer by WordPress
- Remove the wp rest api meta tags from wp_head
- Remove oembed meta tag from head
- Remove rss meta tags from head

#### Emoji's:
*Disable and cleanup all emoji related stuff*

 - Dispable all empoji conversion in any TinyMce wysiwyg editor
 - Remove emoji actions:
    - wp_head -> print_emoji_detection_script
    - wp_print_styles -> print_emoji_styles
    - admin_print_styles -> print_emoji_styles
    - admin_print_scripts -> print_emoji_detection_script
 - Remove emoji filters:
    - wp_mail -> wp_staticize_emoji_for_email
    - the_content_feed -> wp_staticize_emoji
    - comment_text_rss -> wp_staticize_emoji

#### Yoast SEO:
*Clean up several diefrent yoast seo stuff*

 - Move yoast menu item to the bottom of the ammin menu
 - Cleanup dashboard columns from the Yoast SEO plugin
 - Remove the WP SEO columns
 - Cleanup the Yoast SEO dashboard overview widget
 - Hide the yoast seo sidbar ads
 - Disable the intro tour after activating Yoast SEO plugin for the first time
 - Remove all Yoast SEO menu items from the admin bar
 - Replace the Yoast canonical rel link element with a clean version.




## Core

#### Functions:
*Core plugin functions*

 - Customized excerpt loading
 - Get the user roles from a user_id


#### Init:
*Main WordPress Initialization functionality*

 - Adds all new custom welcome widget


#### Rewrites:
*Central custom rewrite rules and redirect functions*

 - Redirect /login to wp-admin or wp-login.php if user is not logged in.
 - Check if the .htaccess is writable, and display an admin notice if it's not writable

#### User:
*Custom Client User role utilities*

 - Get the current user object
 - Check if the current logged in user has 'administrator' user role.
 - Check if the current logged in user has 'client_admin' user role.
 - Check if the current logged in user has 'client_user' user role.

#### Image:
*Image handling with [WpThumb](https://github.com/humanmade/WPThumb)*

- Automatic dynamic images resizing, cropping and caching.
- Save images to cache folders
- Only generate resized images when whey are used (on first request)




## Dashboard
Register and populate custom admin dashboard widgets.

### At a Glance Widget:
*Add all public post types and count to the 'At a glance' admin dashboard widget.*

 - Manipulate the default 'At a glance' dashboard widget to show all public post-types.
 - Manipulate the widget footer content
 - Add some custom css for the widget


### Jold Welcome Widget:
*Improve and change the big welcome admin dashboard widget with custom content, links and text.*

 - WIP: Work In Progress
