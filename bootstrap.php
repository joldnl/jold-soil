<?php

/**
 *
 * Main Soil Bootstrapper
 *
 * Bootstrap all Soil Namesmace and Classes
 *
 * @package      Soil
 * @subpackage   Cleanup
 * @category     Admin Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil;


use Soil\Core;                              // Main plugin Class
use Soil\Core\Init;                         // Main plugin Class
use Soil\Core\Settings;                     // Main plugin Class
use Soil\Core\SettingsVendor;               // Main plugin Class
use Soil\Core\SettingsYoast;                // Main plugin Class
use Soil\Core\SettingsImage;                // Main plugin Class
use Soil\Core\Image;                        // Main plugin Class
// use Soil\Core\ImageOptimization;                        // Main plugin Class
// use Soil\Cli\Assets;                 // Main plugin Class

use Soil\Cleanup;                           // Main plugin Class
use Soil\Cleanup\Admin;                     // Main plugin Class
use Soil\Cleanup\Frontend;                  // Main plugin Class
use Soil\Cleanup\Yoast;                     // Main plugin Class
use Soil\Cleanup\YoastHead;                 // Main plugin Class
use Soil\Cleanup\Emoji;                     // Main plugin Class

use Soil\Dashboard;                         // Main plugin Class
use Soil\Dashboard\WidgetPostcount;         // Main plugin Class
use Soil\Dashboard\WidgetWelcome;           // Main plugin Class

// use \ShortPixel\ShortPixel;



require_once('plugins/wpthumb/wpthumb.php');

require_once('autoload.php');
require_once('vendor/autoload.php');



/**
 * ------------------
 * Core
 * ------------------
 */

// Set up the default Timber context & extend Twig for the site
new Core;
new Core\Init;
new Core\Functions;
new Core\Settings;
new Core\SettingsVendor;
new Core\SettingsYoast;
new Core\SettingsImage;
new Core\Image;
new Core\ImageOptimization;

new Cli\Assets;


new Cleanup\Admin;
new Cleanup\Frontend;
new Cleanup\Yoast;
new Cleanup\YoastHead;
new Cleanup\Emoji;

new Dashboard\WidgetPostcount;
new Dashboard\WidgetWelcome;
