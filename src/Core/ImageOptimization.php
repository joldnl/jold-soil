<?php

/**
 *
 * Image Optimisation methods
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Image
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;

use Soil\Core;
use Soil\Core\Functions;

use WP_Thumb;
use \ShortPixel\ShortPixel;


/**
 * Custom image handling, resizing and caching using WpThumb
 */
class ImageOptimization {


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        require_once( \Soil\Core::plugin_path() . 'vendor/shortpixel/shortpixel-php/lib/ShortPixel.php' );

        \ShortPixel\setKey( get_option('setting_image_shortpixel_key') );


        add_filter( 'cron_schedules',                   [ $this, 'cronAddScheduleFiveMinutes' ] );

        add_action( 'soil_assets_optimization',    [ $this, 'cronExcecute' ] );

        $this->cronRegister();

    }



    /**
     * Add a new cron interval schedule of five minutes
     * @param  array    $schedules   The current registered interval schedules in wp
     * @return array                 Schedules array with the new five minutes interval
     */
    public function cronAddScheduleFiveMinutes( $schedules ) {

        $schedules['soil_assets_optimization'] = array(
            'interval' => 300,
            'display'  => esc_html__( 'Every 5 Minutes' ),
        );

        return $schedules;

    }


    /**
     * Add a new cron interval schedule of 30 minutes
     * @param  array    $schedules   The current registered interval schedules in wp
     * @return array                 Schedules array with the new five minutes interval
     */
    public function cronAddScheduleHalfHour( $schedules ) {

        $schedules['half_hour'] = array(
            'interval' => 1800,
            'display'  => esc_html__( 'Every Half Hour' ),
        );

        return $schedules;

    }



    /**
     * Register the cron schedule for checking en optimizing images
     */
    public function cronRegister() {

        if ( $this->checkOption('setting_image_enable_optimization') != true) {

            // Get timestamp of next optimization event, if it exists
            $timestamp = wp_next_scheduled( 'soil_assets_optimization' );

            // Only deactivate cron schedule if it exsists
            if ($timestamp) {

                // Clear the cron schedule
                wp_clear_scheduled_hook( 'soil_assets_optimization' );

                // Remove the needsOptimization wp option
                delete_option('soilAssetsNeedsOptimization');

                // Report actions to error log
                Functions::log( '[!]  Image optimization is disabled' );
                Functions::log( '     Removing cron schedule ... ' );
                Functions::log( '     Delete wp option ... ' );

            }

        } else {

            // Check if schedule exists
            if ( ! wp_next_scheduled( 'soil_assets_optimization' ) ) {

                // Register cron schedule if it does not exist
                wp_schedule_event( time(), 'soil_assets_optimization', 'soil_assets_optimization' );

                // Report actions to error log
                Functions::log( '[!]  Register cron schedule' );
                Functions::log( '     WP Cron task `soil_assets_optimization` is now scheduled' );

            }

        }

    }


    /**
     * Excecute image optimization wp cron task
     * @return n/a    Invokes the image optimization process
     */
    public function cronExcecute() {
        Functions::log( '[!]  Excecute image optimization cron' );
        $optim = new ImageOptimization;
        $optim->optimizeAssets();
    }


    /**
     * Get the location (dir) of the wp uploads folder
     * @return string   The dir of the uploads folder
     */
    private function getAssetsDir() {
        $assets_path = wp_upload_dir()['path'];
        return $assets_path;
    }


    /**
     * Get the cache folder contents
     * @return array    An array with all cache subfolders
     */
    private function getCacheDir() {
        $cache_dir = glob($this->getAssetsDir() . '/cache/*' , GLOB_ONLYDIR);
        return $cache_dir;
    }


    /**
     * Count the total number of cache folders
     * @return integer  returns the number of cache folders
     */
    public function getCacheFolderCount() {

        $folders = $this->getCacheDir();

        if ( isset($folders) && !empty($folders) && is_array($folders) ) {
            return count($folders);
        }

        return false;

    }


    /**
     * Check if an -optimized.webp version of the file exists
     * @param  string   $dir    The directory path of the cache folder
     * @param  string   $file   The filename to look for
     * @return boolean          Returns true if a optimized webp version exists
     */
    private function getOptimizedFile( $dir = null, $file = null, $extension = null ) {

        if ( isset($dir) && $dir !== null && isset($file) && $file !== null && isset($extension) && $extension !== null  ) {

            $filename   = $this->getOptimizedPath($dir, $file, $extension);
            $isWepb     = (file_exists($filename) ? true : false);

            if ( $isWepb ) {

                return true;

            }

        } else {

            // Throw an exception if $string or $characters are not set or false
            Functions::log('[X]  Function checkWebP expects 3 parameters');
            Functions::log('     Not all the parameters are set');


        }

        return false;

    }


    /**
     * get the filename of an optimized version
     * @param  string   $dir    The directory path of the cache folder
     * @param  string   $file   The filename to look for
     * @return boolean          Returns true if a optimized webp version exists
     */
    private function getOptimizedFileName( $dir = null, $file = null, $extension = null ) {

        if ( isset($dir) && $dir !== null && isset($file) && $file !== null && isset($extension) && $extension !== null  ) {

            $filename   = $this->getOptimizedPath($dir, $file, $extension);
            $isWepb     = (file_exists($filename) ? true : false);

            if ( $isWepb ) {

                return true;

            }

        } else {

            // Throw an exception if $string or $characters are not set or false
            Functions::log('Function checkWebP expects both $dir, $file and $extension parameters, one of them is not set.');

        }

        return false;

    }


    /**
     * Create a optimized version of the file path
     * @param  string   $dir    The directory path of the cache folder
     * @param  string   $file   The filename to look for
     * @return boolean          Returns an file path with optimized suffix
     */
    private function getOptimizedPath( $dir = null, $file = null, $extension = null ) {

        if ( isset($dir) && $dir !== null && isset($file) && $file !== null && isset($extension) && $extension !== null ) {

            $dir    = $dir . '/' . $file;
            $parts  = pathinfo($dir);
            $path   = $parts['dirname'] . '/' . $parts['filename'] . '-optimized.' . $extension;

            return $path;

        }

        return false;

    }


    /**
     * Get filesize details of the original, and optimized versions of an asset
     * @param  string   $originalPath    The path of the original asset
     * @param  string   $jpgPath         The path to the optimized jpg
     * @param  string   $webpPath        The path to the optimized webp
     * @return array                     Returns array with the filesizes and comparisons
     */
    private function getFilesizeDetails( $originalPath = null, $jpgPath = null, $webpPath = null ) {

        // TODO: Check for each path, and return error if path is fucked
        if (
            isset($originalPath) && $originalPath !== null && file_exists($originalPath) &&
            isset($jpgPath) && $jpgPath !== null && file_exists($jpgPath) &&
            isset($webpPath) && $webpPath !== null && file_exists($webpPath)
        ) {

            $original   = filesize($originalPath);
            $jpg        = filesize($jpgPath);
            $webp       = filesize($webpPath);
            $pctJpg     = (1 - $jpg / $original) * 100;
            $pctWebp    = (1 - $webp / $original) * 100;

            $details  = [
                'cached' => Functions::fileSizeConvert($original),
                'jpg'    => [
                    'size'  => Functions::fileSizeConvert($jpg),
                    'saved' => Functions::fileSizeConvert(($original - $jpg)),
                    'percentage' => number_format($pctJpg, 0) . '%',
                ],
                'webp'   => [
                    'size'  => Functions::fileSizeConvert($webp),
                    'saved' => Functions::fileSizeConvert(($original - $webp)),
                    'percentage' => number_format($pctWebp, 0) . '%',
                ],
            ];

            return $details;

        }

        return false;

    }


    /**
     * Loop trough all original cached (wpthumb generated) asset files and generate optimized versions (webp, jpg) trough ShortPixel API
     * @return string   The cli output table with assets file details
     */
    public function optimizeAssets( $dryrun = false ) {

        $this->checkOptionNeedsOptimization();

        /**
         * Check of optimization is needed at all (check wp-setting `soilAssetsNeedsOptimization`)
         */
        if ( $this->getOptionNeedsOptimization() === 'no' ) {

            Functions::log( "[!]  Abort Image Optimization:" );
            Functions::log( "     All assets are optimized!" );
            return false;

        }


        /**
         * Check of 'setting_image_shortpixel_key' is set
         */
        if ( empty($this->checkOption('setting_image_shortpixel_key')) || $this->checkOption('setting_image_shortpixel_key') == false) {

            Functions::log( "[!]  Abort Image Optimization:" );
            Functions::log( "     ShortPixel Key is not set!" );
            return false;

        }


        /**
         * Check of 'setting_image_enable_optimization' is enabled
         */
        if ( $this->checkOption('setting_image_enable_optimization') != true) {

            Functions::log( "[!]  Abort Image Optimization:" );
            Functions::log( "     Image optimization is disabled in Soil Settings." );
            return false;

        }

        Functions::log( '[!]  Starting Image Optimization ...' );
        Functions::log( "+---------------------------------------------------------------------------+--------+-------------+" );
        Functions::log( "| Optimize Asset File                                                       | to ->  | Status      |" );
        Functions::log( "+---------------------------------------------------------------------------+--------+-------------+" );

        // Get the wp-uploads dir path
        $assets_path = $this->getAssetsDir();

        // Get the cache folder contents
        $cache_dir   = glob($assets_path . '/cache/*' , GLOB_ONLYDIR);

        $f = 0;

        if ( $this->checkOption('setting_image_enable_compression') == false) {
            Functions::log( '[X]  Optimizing JPG images is disabled in soil settings' );
        }

        if ( $this->checkOption('setting_image_enable_webp') == false) {
            Functions::log( '[X]  Creating WebP images is disabled in soil settings' );
        }

        foreach ($cache_dir as $dir) {

            // Write output table header
            $dirSections = explode('/', $dir);

            // Write output table header
            $dirName     = end($dirSections);

            // Write output table header
            $files       = array_diff(scandir($dir), array('.', '..', '.svn', '.htaccess', '.DS_Store'));

            $i=0;

            // Loop trough all the files in the cache folder
            foreach ($files as $file) {

                $path_parts         = pathinfo($file);
                $fileSections       = explode('/', $file[2]);

                // Only get original files, not the already optimized ones
                if ( !strpos($path_parts['filename'], '-optimized') ) {

                    // Check if optimized files already exists, of not, start to generate new ones
                    if ( !$this->getOptimizedFile($dir, $file, $path_parts['extension']) || !$this->getOptimizedFile($dir, $file, 'webp') ) {

                        /**
                         * Check of 'setting_image_enable_compression' is enabled
                         */
                        if ( $this->checkOption('setting_image_enable_compression') == true) {

                            // Generate JPG optimized
                            if ( !$this->getOptimizedFile($dir, $file, $path_parts['extension']) ) {
                                $this->optimizeFile($dir . '/' . $file, 'jpg');
                            }

                        } else {
                            // Functions::log( '[X]  Optimizing JPG images is disabled in soil settings. ' );
                        }


                        /**
                         * Check of 'setting_image_enable_webp' is enabled
                         */
                        if ( $this->checkOption('setting_image_enable_webp') == true) {

                            // Generate WebP file
                            if ( !$this->getOptimizedFile($dir, $file, 'webp') ) {
                                $this->optimizeFile($dir . '/' . $file, 'webp');
                            }

                        } else {
                            // Functions::log( '[X]  Creating WebP images is disabled in soil settings. ' );
                        }

                        Functions::log( "+---------------------------------------------------------------------------+--------+-------------+" );

                    }


                }

                $f++;
                $i++;

            }

        }

        Functions::log( '[V]  Done optimizing assets!' );

    }



    /**
     * Generate an optimized .jpg file to disk cache trough ShortPixel
     * @return string   Reports back to the log with a status
     */
    private function optimizeFile( $file, $extension, $dryrun = false ) {

        $fileInfo = pathinfo( $file );
        $path     = $this->getOptimizedPath( $fileInfo['dirname'], $fileInfo['filename'], $fileInfo['extension']);
        $pathInfo = pathinfo($path);

        if ( class_exists('\ShortPixel\ShortPixel') ) {

            if ($dryrun == true) {
                $succeeded = true;
            } else {
                if ($extension == 'webp') {
                    $ret = \ShortPixel\fromFile( $file )->generateWebP()->toFiles( $pathInfo['dirname'], $pathInfo['basename'] );
                }
                if ($extension == 'jpg') {
                    $ret = \ShortPixel\fromFile( $file )->toFiles( $pathInfo['dirname'] , $pathInfo['basename'] );
                }

                if ( isset( $ret->succeeded[0] ) ) {
                    $succeeded = true;
                } else {
                    $succeeded = false;
                }
            }

            $this->logOptimization( $fileInfo, $extension, $succeeded );


        } else {

            // Throw an exception if $string or $characters are not set or false
            Functions::log( 'Class \ShortPixel\ShortPixel not found.' );

        }

    }


    /**
     * Display a table with all assets in cache, and check if they are optimized
     * @return string   The cli output table with assets file details
     */
    public function assetsDetails() {

        // Get the wp-uploads dir path
        $assets_path = $this->getAssetsDir();

        // Get the cache folder contents
        $cache_dir   = $this->getCacheDir();

        $output = [];

        $f = 0;

        foreach ($cache_dir as $dir) {

            // Explode directory path into an array
            $dirSections = explode('/', $dir);

            // Get the cache folder name
            $dirName     = end($dirSections);

            // Get all files except system and ignored files
            $files       = array_diff(scandir($dir), array('.', '..', '.svn', '.htaccess', '.DS_Store'));

            $i=0;

            // Loop trough all the files in the cache folder
            foreach ($files as $file) {

                $path_parts         = pathinfo($file);
                $fileSections       = explode('/', $file[2]);

                // Only get original files, not the already optimized ones
                if ( !strpos($path_parts['filename'], '-optimized') ) {

                    $filesizeDetails    = $this->getFilesizeDetails(
                        $dir . '/' . $file,
                        $this->getOptimizedPath($dir, $file, $path_parts['extension']),
                        $this->getOptimizedPath($dir, $file, 'webp')
                    );

                    $details = [
                        'cacheId'       => $dirName,
                        'filename'      => $path_parts['filename'] . '.' . $path_parts['extension'],
                        'optinizedJpg'  => $this->getOptimizedFile($dir, $file, $path_parts['extension']),
                        'optinizedWebp' => $this->getOptimizedFile($dir, $file, 'webp'),
                        'sizeDetails'   => $filesizeDetails,
                    ];

                    $output[] = $details;

                }

                $f++; $i++;

            }

        }

        return $output;

    }


    /**
     * Get details of the optimized assets, and ther counts
     * @return array    Details about the assets, and optimized versions
     */
    public function assetsSummary() {

        $files      = $this->assetsDetails();
        $jpgCount   = 0;
        $webpCount  = 0;

        foreach ($files as $file) {

            if ( $file['optinizedJpg'] ) {
                $jpgCount++;
            }

            if ( $file['optinizedWebp'] ) {
                $webpCount++;
            }

        }

        $output = [
            'cacheFolders'  => $this->getCacheFolderCount(),
            'assetsTotal'   => count($files),
            'assetsJpg'     => $jpgCount,
            'assetsWebp'    => $webpCount,

        ];

        return $output;

    }


    /**
     * Check if assets generation is needed. Compare jpg's and webp's to nr of original assets
     * @return boolean  returns true not all assets are optimized
     */
    public function checkOptionNeedsOptimization() {

        // Get assets summary details
        $details = $this->assetsSummary();

        // Check if number of jpgs and number of webp files is equal to total original assets.
        if ( $details['assetsTotal'] == $details['assetsJpg'] && $details['assetsTotal'] == $details['assetsWebp'] ) {

            // Set option to 'no' if it's not allready 'no'
            if ($this->getOptionNeedsOptimization() != 'no' ) {
                $this->setOptionNeedsOptimization( "no" );
                // Functions::log('Setting `soilAssetsNeedsOptimization` to "' . $this->getOptionNeedsOptimization() . '"');
            }

        } else {
            // Set option to 'yes'
            $this->setOptionNeedsOptimization( "yes" );
            // Functions::log('Setting `soilAssetsNeedsOptimization` to "' . $this->getOptionNeedsOptimization() . '"');
        }

        return $this->getOptionNeedsOptimization();

    }


    /**
     * Check if optimization is needed by checking the wp-option `soilAssetsNeedsOptimization`
     * @return  boolean   Returns true of not all assets are optimized
     */
    public function getOptionNeedsOptimization() {
        return get_option('soilAssetsNeedsOptimization');
    }


    /**
     * Set wp-option `soilAssetsNeedsOptimization`
     */
    public function setOptionNeedsOptimization( $value ) {
        update_option('soilAssetsNeedsOptimization', $value, true);
    }


    /**
     * Output some logging details about the optimization process
     * @param  array    $path_parts     Array with the path parts
     * @param  string   $type           The image type to optimize
     * @param  boolean  $succeeded      True if the optimization is succesful
     * @return string                   Returns the log output to
     */
    private function logOptimization( $path = null, $type = null, $succeeded = null ) {

        // Get the optimized file path
        $optimized = pathinfo( $this->getOptimizedPath( $path['dirname'], $path['filename'], $type) );


        // Prepare status
        if ( isset($succeeded) && $succeeded == true ) {
            $success = '[V] Passed ';
        } else {
            $success = '[X] Failed ';
        }

        // Write optimization status to log
        Functions::log( '| ' . Functions::fixedString($path['basename'], 73) . ' | ' . Functions::fixedString('.' . $type, 6) . ' | ' . $success . ' |' );

    }


    /**
     * Check if a wordpress setting exists
     * @param  string   $optionKey   The key of the option from the options table
     * @return mixed                 Returns the option, or false of the option doesnt exists
     */
    private function checkOption( $optionKey = null ) {

        if ( isset($optionKey) && $optionKey !== null ) {

            $option = get_option($optionKey);

            if ( !empty($option) ) {
                return $option;
            }

        }

        return false;

    }


}
