<?php

/**
 *
 * Soil settings configuration class
 *
 * Yoast settings class
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Settings
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;

use Soil\Core;
use Soil\Core\Functions;


/**
 * Admin init functions
 */
class SettingsYoast {


    public $settings;
    public $options_page;


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {


        /**
         * Default settings page options
         * @var array
         */
        $this->options_page = [
            'id'            => 'soil-settings-yoast-page',
            'page_title'    => __( 'Soil Yoast SEO Cleanup', 'joldnl-soil' ),
            'menu_title'    => __( 'Soil Yoast SEO Cleanup', 'joldnl-soil' ),
        ];

        add_action( 'admin_init',       array( $this, 'settings_init' ) );

    }



    /**
     *
     * settings_init
     *
     * Initiate and register settings sections and fields
     *
     * @return  n/a
     *
     */
    function settings_init() {

        /**
         * Soil Yoast Cleanup Section
         */
        add_settings_section(
            'section_soil_yoast',                                // $id          ID used to identify this section and with which to register options
            __('Cleanup Yoast SEO options', 'joldnl-soil'),      // $title       Title to be displayed on the administration page
            [$this, 'settings_section'],                         // $callback    Callback used to render the description of the section
            'soil-settings-yoast-page'                           // $page        Page on which to add this section of options
        );


        /**
         * Clean columns
         */
        add_settings_field(
            'soil_yoast_clean_columns',                          // $id          ID used to identify the field throughout the theme
            __('Clean columns', 'joldnl-soil'),                  // $title       The label to the left of the option interface element
            [$this, 'setting_yoast_clean_columns'],              // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-yoast-page',                          // $page        The page on which this option will be displayed
            'section_soil_yoast',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Cleanup dashboard columns from the Yoast SEO plugin', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-yoast-page', 'soil_yoast_clean_columns' );


        /**
         * dashboard_widget
         */
        add_settings_field(
            'soil_yoast_hide_dashboard_widget',                  // $id          ID used to identify the field throughout the theme
            __('Remove Dashboard Widget', 'joldnl-soil'),        // $title       The label to the left of the option interface element
            [$this, 'setting_yoast_hide_dashboard_widget'],      // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-yoast-page',                          // $page        The page on which this option will be displayed
            'section_soil_yoast',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Cleanup the Yoast SEO dashboard overview widget', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-yoast-page', 'soil_yoast_hide_dashboard_widget' );


        /**
         * sidebar_ads
         */
        add_settings_field(
            'soil_yoast_hide_sidebar_ads',                       // $id          ID used to identify the field throughout the theme
            __('Remove sidebar adds', 'joldnl-soil'),            // $title       The label to the left of the option interface element
            [$this, 'setting_yoast_hide_sidebar_ads'],           // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-yoast-page',                          // $page        The page on which this option will be displayed
            'section_soil_yoast',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Hide all the yoast seo sidbar ads', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-yoast-page', 'soil_yoast_hide_sidebar_ads' );


        /**
         * ignore_tour
         */
        add_settings_field(
            'soil_yoast_ignore_tour',                            // $id          ID used to identify the field throughout the theme
            __('Disable tour', 'joldnl-soil'),                   // $title       The label to the left of the option interface element
            [$this, 'setting_yoast_ignore_tour'],                // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-yoast-page',                          // $page        The page on which this option will be displayed
            'section_soil_yoast',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Disable the intro tour after activating Yoast SEO plugin for the first time', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-yoast-page', 'soil_yoast_ignore_tour' );


        /**
         * remove_adminbar
         */
        add_settings_field(
            'soil_yoast_remove_adminbar',                        // $id          ID used to identify the field throughout the theme
            __('Remove adminbar', 'joldnl-soil'),                // $title       The label to the left of the option interface element
            [$this, 'setting_yoast_remove_adminbar'],            // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-yoast-page',                          // $page        The page on which this option will be displayed
            'section_soil_yoast',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Remove all Yoast SEO menu items from the admin bar', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-yoast-page', 'soil_yoast_remove_adminbar' );


    }



    /* ------------------------------------------------------------------------ *
     * Section Callbacks
     * ------------------------------------------------------------------------ */
    function settings_section() {
        echo '<p>These settins are used on the dashboard welcoe widget.</p>';
    }

    function setting_yoast_clean_columns($args) {
        $html  = '<input type="checkbox" id="soil_yoast_clean_columns" name="soil_yoast_clean_columns" value="true" ' . checked( 'true', get_option('soil_yoast_clean_columns'), false ) . '/>';
        $html .= '<label for="soil_yoast_clean_columns"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_yoast_hide_dashboard_widget($args) {
        $html  = '<input type="checkbox" id="soil_yoast_hide_dashboard_widget" name="soil_yoast_hide_dashboard_widget" value="true" ' . checked( 'true', get_option('soil_yoast_hide_dashboard_widget'), false ) . '/>';
        $html .= '<label for="soil_yoast_hide_dashboard_widget"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_yoast_hide_sidebar_ads($args) {
        $html  = '<input type="checkbox" id="soil_yoast_hide_sidebar_ads" name="soil_yoast_hide_sidebar_ads" value="true" ' . checked( 'true', get_option('soil_yoast_hide_sidebar_ads'), false ) . '/>';
        $html .= '<label for="soil_yoast_hide_sidebar_ads"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_yoast_ignore_tour($args) {
        $html  = '<input type="checkbox" id="soil_yoast_ignore_tour" name="soil_yoast_ignore_tour" value="true" ' . checked( 'true', get_option('soil_yoast_ignore_tour'), false ) . '/>';
        $html .= '<label for="soil_yoast_ignore_tour"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_yoast_remove_adminbar($args) {
        $html  = '<input type="checkbox" id="soil_yoast_remove_adminbar" name="soil_yoast_remove_adminbar" value="true" ' . checked( 'true', get_option('soil_yoast_remove_adminbar'), false ) . '/>';
        $html .= '<label for="soil_yoast_remove_adminbar"> '  . $args[0] . '</label>';
        echo $html;
    }




    function settings_page_content() {

        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }

        // check if the user have submitted the settings
        // wordpress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            // add_settings_error( 'soil-settings_messages', 'soil-settings_message', __( 'Settings Saved', 'joldnl-soil' ), 'updated' );
        }

        // show error/update messages
        settings_errors( 'soil-settings_messages' );

        // include_once( Core::plugin_path() . 'views/AdminSettings.php' );
        require( Core::plugin_path() . 'views/AdminSettings.php' );

    }




}
