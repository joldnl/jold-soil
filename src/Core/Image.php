<?php

/**
 *
 * Image handling with WpThumb
 *
 * Custom image handling, resizing and caching using WpThumb
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Image
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;

use Soil\Core;
use Soil\Core\Functions;

use WP_Thumb;
use \ShortPixel\ShortPixel;


/**
 * Custom image handling, resizing and caching using WpThumb
 */
class Image {


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        // Run wp actions
        add_action( 'after_setup_theme',                    array( $this, 'image_settings' ) );                 // Set default media settings

        // Run wp filters
        add_filter( 'intermediate_image_sizes',             array( $this, 'delete_image_sizes' ) );             // Remove deault image sizes
        add_filter( 'intermediate_image_sizes_advanced',    array( $this, 'disable_wp_image_generatetion' ) );  // Prevent WP from generating resized images on upload (for both default and custom image-sizes)
        add_filter( 'wp_generate_attachment_metadata',      array( $this, 'force_metadata') );                  // Trick WP into thinking images were generated anyway (by addidng meta data)
        add_filter( 'wpthumb_create_args_from_size',        array( $this, 'force_wpthumb') );                   // Also use WPThumb for the all default sizes (except full)
        add_filter( 'image_resize_dimensions',              array( $this, 'force_retina_images'), 10, 6);       // Force upscale images when needed
        add_filter( 'image_downsize',                       array( $this, 'wpimg_to_wpthumb') , 99, 3 );
        add_filter( 'jpeg_quality',                         array( $this, 'image_quality' ) );
        add_filter( 'sanitize_file_name',                   array( $this, 'sanitize_filemane' ), 10);

    }



	/**
     *
     * image_settings
     *
     * Disable uploads in year/month structure, save all in one folde.
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function image_settings() {

        // Disable uploads in year/month structure, save all in one folde.
        update_option( 'uploads_use_yearmonth_folders', 0 );

    }



    /**
     *
     * sanitize_filemane
     *
     * Sanitize file filenames on upload
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   string  $imagefile   The original filename
     *
     * @return  string  $sanitized   The sanitized filename
     *
     */
    public function sanitize_filemane( $imagefile ) {

        // Convert filename to ascii format
        $sanitized = remove_accents( $imagefile );

        // Invalid characters to be replaced
        $invalid = array(
            ' '     => '-',
            '%20'   => '-',
            '_'     => '-'
        );

        // Sanitize the filename
        $sanitized = str_replace( array_keys($invalid), array_values($invalid), $sanitized );   // Replace invalid characters
        $sanitized = preg_replace( '/[^A-Za-z0-9-\. ]/', '', $sanitized );                      // Remove all non-alphanumeric except .
        $sanitized = preg_replace( '/\.(?=.*\.)/', '', $sanitized );                            // Remove all but last .
        $sanitized = preg_replace( '/-+/', '-', $sanitized );                                   // Replace any more than one - in a row
        $sanitized = str_replace( '-.', '.', $sanitized );                                      // Remove - from end of filename
        $sanitized = strtolower( $sanitized );                                                  // Set filename to lowercase

        return $sanitized;

    }



    /**
     *
     * image_quality
     *
     * Get custom image quality, or default to 60%
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   string  $quality    The original filename
     *
     * @return  string  $quality    The image quality, either the provided quality, or 60 if none is set
     *
     */
    public function image_quality( $quality ) {

        if ( isset($quality) && !empty($quality) ) {

            return $quality;

        }

        // image quality 60% if none privided
        return 60;

    }



    /**
     *
     * delete_image_sizes
     *
     * Disable deault WordPress medium and large image sizes
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   array   $image_sizes    All registered images sizes
     *
     * @return  string  $image_sizes    Images sizes without default medium and loarge
     *
     */
    public function delete_image_sizes( $image_sizes ) {

        unset( $image_sizes[1] ); // image medium
        unset( $image_sizes[2] ); // image large

        return $image_sizes;

    }



    /**
     *
     * thumbnail_sizes
     *
     * Get image size info (default and via add_image_size())
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @return  array  $sizes    Images sizes per image size
     *
     */
    private function thumbnail_sizes() {

        global $_wp_additional_image_sizes;

        $sizes = array();
        $intermediate_sizes = get_intermediate_image_sizes();

        // Create the full array with sizes and crop info
        foreach( $intermediate_sizes as $_size ) {

            if ( in_array( $_size, array( 'thumbnail', 'large' ) ) ) {

                $sizes[ $_size ]['width']   = get_option( $_size . '_size_w' );
                $sizes[ $_size ]['height']  = get_option( $_size . '_size_h' );
                $sizes[ $_size ]['crop']    = (bool) get_option( $_size . '_crop' );

            }

            elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

                $sizes[ $_size ] = array(
                    'width'     => $_wp_additional_image_sizes[ $_size ]['width'],
                    'height'    => $_wp_additional_image_sizes[ $_size ]['height'],
                    'crop'      => $_wp_additional_image_sizes[ $_size ]['crop'],
                );

            }

        }

        return $sizes;

    }



    /**
     *
     * wpimg_to_wpthumb
     *
     * Turn all image sizes into dynmaic WPThumb images
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   array  $null    Images sizes per image size
     * @param   array  $id      Images sizes per image size
     * @param   array  $args    Images sizes per image size
     *
     * @return  array           WpThumb image
     *
     */
    function wpimg_to_wpthumb( $null, $id, $args ) {

        // Do not run this function when in the admin dashboard
        $from_frontend = isset( $_REQUEST['frontend_ajax'] ) && ( TRUE === (bool) $_REQUEST['frontend_ajax'] );

        if ( defined('DOING_AJAX') && DOING_AJAX && is_admin() && !$from_frontend ) {
        // if ( defined('DOING_AJAX') || is_admin() ) {

            return $null;

        }

        if (is_admin()) {
            return $null;
        }

        $oriargs = $args;

        // check if $args is a WP Thumb argument list, or native WordPress one wp thumb looks like this: 'width=300&height=120&crop=1' native looks like 'thumbnail'
        if ( is_string( $args ) && ! strpos( (string) $args, '=' ) ) {

            if( $args == 'full' || $args == 'thumbnail' ) {

                return false;

            }

            // If this is a  WP image size. Convert it to WPThumb args
            $args = apply_filters( 'wpthumb_create_args_from_size', $args );

            // if there are no "special" wpthumb args, then we shouldn' bother
            // creating a WP Thumb, just use the WordPress one
            // if ( $args === ( $args = apply_filters( 'wpthumb_create_args_from_size', $args ) ) ) {
            //     return $null;
            // }

        }

        $args = wp_parse_args( $args );

        // Set width
        if ( !empty( $args[0] ) )
            $args['width'] = $args[0];

        // Set height
        if ( !empty( $args[1] ) )
            $args['height'] = $args[1];

        // Set Crop
        if ( !empty( $args['crop'] ) && $args['crop'] && empty( $args['crop_from_position'] ) )
            $args['crop_from_position'] = get_post_meta( $id, 'wpthumb_crop_pos', true );

        // Get path by id if path is empty
        if ( empty( $path ) )
            $path = get_attached_file( $id );


        $path   = apply_filters( 'wpthumb_post_image_path', $path, $id, $args );
        $args   = apply_filters( 'wpthumb_post_image_args', $args, $id );
        $image  = new WP_Thumb( $path, $args );
        $args   = $image->getArgs();

        // Functions::log( $image->getCacheFilePath() );
        // if ( !file_exists($image->getCacheFilePath()) ) {
        //
        //   $fileDetails    = pathinfo( $image->getCacheFilePath() );
        //   $fileDetailsDir = $fileDetails['dirname'];
        //   $dirSections    = explode('/', $fileDetailsDir);
        //   $cacheFolder    = end($dirSections);
        //
        //   Functions::log( '[!]  WpThump cache: ' . $cacheFolder . '/' . $fileDetails['filename'] . '.' . $fileDetails['extension'] );
        //
        // }


        extract( $args );


        if ( !$image->errored() ) {


            // Original image
            $image_src  = $image->returnImage();


            // Check if Optimization is enabled at all
            if ( get_option('setting_image_enable_optimization') ) {

                if ( class_exists('\ShortPixel\ShortPixel') ) {

                    // Get the ShortPixel API
                    \ShortPixel\setKey( get_option('setting_image_shortpixel_key') );

                    // Return the compressed jpg filename
                    if ( get_option('setting_image_show_compressed') ) {

                        $filePath        = pathinfo( $image->getCacheFilePath() );
                        $fileUrl         = pathinfo( $image->returnImage() );
                        $optimizedPath   = $filePath['dirname'] . '/' . $filePath['filename'] . '-optimized.' . $filePath['extension'];
                        $optimizedExists = file_exists($optimizedPath);

                        if ( $optimizedExists ) {
                            $image_src = $fileUrl['dirname'] . '/' . $fileUrl['filename'] . '-optimized.' . $fileUrl['extension'];
                        }

                    }

                    // Return the compressed WebP filename
                    if ( get_option('setting_image_show_webp') ) {

                        if( strpos( $_SERVER['HTTP_ACCEPT'], 'image/webp' ) !== false ) {

                            $filePath        = pathinfo( $image->getCacheFilePath() );
                            $fileUrl         = pathinfo( $image->returnImage() );
                            $optimizedPath   = $filePath['dirname'] . '/' . $filePath['filename'] . '-optimized.webp';
                            $optimizedExists = file_exists($optimizedPath);

                            if ( $optimizedExists ) {
                                $image_src = $fileUrl['dirname'] . '/' . $fileUrl['filename'] . '-optimized.webp';
                            }

                        }

                    }

                }

            }


            $crop       = (bool) ( empty( $crop ) ) ? false : $crop;

            if ( !$image->errored() && $image_meta = @getimagesize( $image->getCacheFilePath() ) ) {
                $html_width  = $image_meta[0];
                $html_height = $image_meta[1];
            }

            else {
                $html_width  = $html_height = false;
            }

        } else {
            $html_width      = $width;
            $html_height     = $height;
            $image_src       = $image->getFileURL();
        }

        return array( $image_src, $html_width, $html_height, true );

    }



    /**
     *
     * wpimg_to_wpthumb
     *
     * Use WPThumb for the all default sizes, except full, thumbnail or avatar.
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   string    $size    Image size
     *
     * @return  string    $size    Image size
     *
     */
    public function force_wpthumb( $size ) {

        // Chceck if image size is not full or another default wp image size
        if ( $size != 'full' && $size != 'thumbnail' && $size != 'avatar' && $size != 'original' ) {

            $sizes = $this->thumbnail_sizes();

            if( !is_array($size) ) {
                $size_sizes = $sizes[ $size ];
            }

            if( isset( $size_sizes ) && is_array( $size_sizes ) ) {
                $size = 'width=' . $size_sizes['width'] . '&height=' . $size_sizes['height'] . '&crop=1';
            }

            return $size;

        } else {

            return $size;

        }

    }



    /**
     *
     * disable_wp_image_generatetion
     *
     * Prevent WP from generating resized images on upload (for both default and custom image-sizes)
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   string    $sizes    Image size
     *
     * @return  string    		    Image size
     *
     */
    public function disable_wp_image_generatetion( $sizes ) {

        global $rodesk_image_sizes;

        // Save the sizes to a global
        // Because the next function needs them to lie to WP about what sizes were generated
        $rodesk_image_sizes = $sizes;

        // Force WP to not make sizes by telling it there's no sizes to make. Except the thubnail for in the Wp gallery
        return array( "thumbnail" => $sizes['thumbnail'] );

    }



    /**
     *
     * force_metadata
     *
     * Trick WP into thinking images were generated anyway (by addidng meta data)
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   array     $meta    Image metadata
     *
     * @return  array     $meta    Manipulated image metadata
     *
     */
    public function force_metadata( $meta ) {

        global $rodesk_image_sizes;

        // If $rodesk_image_sizes is not defined it means we are uploading something other than an image
        if ( empty( $rodesk_image_sizes ) || ! is_array( $rodesk_image_sizes ) ) {
            return $meta;
        }

        foreach ($rodesk_image_sizes as $sizename => $size) {

            // Figure out what size WP would make this:
            $newsize = image_resize_dimensions( $meta['width'], $meta['height'], $size['width'], $size['height'], $size['crop']);

            if ($newsize) {

                $info       = pathinfo($meta['file']);
                $ext        = $info['extension'];
                $name       = wp_basename($meta['file'], ".$ext");
                $suffix     = "{$newsize[4]}x{$newsize[5]}";
                $orgfile    = "{$name}.{$ext}";
                $newfile    = "{$name}-{$suffix}.{$ext}";
                $upload_dir = wp_upload_dir();

                // If this is not a thumbnail always return original image name
                if( $sizename != 'thumbnail' ) {

                    $filename = $orgfile;

                // If this is a thumbnail return new (formatted) image name
                } else {

                    // Check if the formatted thumbnail image excists
                    if( file_exists( $upload_dir['path'] . '/' . $newfile ) ) {

                        $filename = $newfile;
                        // Else return original image format

                    } else {

                        $filename = $orgfile;

                    }

                }

                // Build the fake meta entry for the size in question
                $resized = array(
                    'file'      => $filename,
                    'width'     => $newsize[4],
                    'height'    => $newsize[5],
                );

                $meta['sizes'][$sizename] = $resized;

            }

        }

        return $meta;

    }



    /**
     *
     * force_retina_images
     *
     * Force retina size upscale genrate images if needed
     * @see     http://wordpress.stackexchange.com/questions/50649/how-to-scale-up-featured-post-thumbnail
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.9
     * @author  Jurgen Oldenburg
     *
     * @param   string     $default
     * @param   string     $orig_w
     * @param   string     $orig_h
     * @param   string     $dest_w
     * @param   string     $dest_h
     * @param   string     $crop
     *
     * @return  array     Manipulated image metadata
     *
     */
    public function force_retina_images( $default, $orig_w, $orig_h, $dest_w, $dest_h, $crop ) {

        if ( !$crop ) return null; // let the wordpress default function handle this

        $aspect_ratio   = $orig_w / $orig_h;
        $size_ratio     = max( $dest_w / $orig_w, $dest_h / $orig_h );

        $crop_w         = round( $dest_w / $size_ratio );
        $crop_h         = round( $dest_h / $size_ratio );

        $s_x            = floor( ($orig_w - $crop_w ) / 2 );
        $s_y            = floor( ($orig_h - $crop_h ) / 2 );

        return array( 0, 0, (int) $s_x, (int) $s_y, (int) $dest_w, (int) $dest_h, (int) $crop_w, (int) $crop_h );

    }



    public function get_shortpix_api_stats( $status ) {

        if ( $status['code'] == 1 ) {
            $return['status'] = 'Success';
            $return['oldImg'] = 'OriginalFile';
            $return['newImg'] = 'SavedFile';
        }


    }




}
