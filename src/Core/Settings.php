<?php

/**
 *
 * Soil settings configuration class
 *
 * Main configuration class
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;

use Soil\Core;
use Soil\Core\Functions;


/**
 * Admin init functions
 */
class Settings {


    public $settings;
    public $options_page;


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {


        /**
         * Populate the WDCD Admin Settings
         * @var array
         */
        $this->options_page = [
            'id'            => 'soil-settings-page',
            'page_title'    => __( 'Soil Settings', 'joldnl-soil' ),
            'menu_title'    => __( 'Soil Settings', 'joldnl-soil' ),
        ];


        add_action( 'admin_init',       array( $this, 'settings_init' ) );
        add_action( 'admin_menu',       array( $this, 'settings_page_register' ) );

    }



    /**
     *
     * settings_page_register
     *
     * Register the theme settings page.
     *
     * @type	function
     * @date	2017/02/21
     * @author  Jurgen Oldenburg
     * @since	1.0.0-alpha
     *
     * @return  n/a
     *
     */
    public function settings_page_register() {

        add_options_page(
            $this->options_page['page_title'],
            $this->options_page['menu_title'],
            'manage_options',
            $this->options_page['id'],
            array( $this, 'settings_page_content')
        );

    }





    function settings_init() {


        add_settings_section(
            'section_soil_general',                         // ID used to identify this section and with which to register options
            __('General Soil configuration options', 'joldnl-soil'),         // Title to be displayed on the administration page
            array( $this, 'settings_section' ),             // Callback used to render the description of the section
            'soil-settings-page'                            // Page on which to add this section of options
        );



        add_settings_field(
            'soil_hide_adminbar',                           // ID used to identify the field throughout the theme
            __('Hide Adminbar', 'joldnl-soil'),              // The label to the left of the option interface element
            array( $this, 'setting_adminbar' ),             // The name of the function responsible for rendering the option interface
            'soil-settings-page',                           // The page on which this option will be displayed
            'section_soil_general',                         // The name of the section to which this field belongs
            array(                                          // The array of arguments to pass to the callback. In this case, just a description.
                __('Disable admin-bar for all users, and remove the setting from the profile page', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-page', 'soil_hide_adminbar' );



        add_settings_field(
            'soil_hide_posts',                              // ID used to identify the field throughout the theme
            __('Hide Posts', 'joldnl-soil'),                 // The label to the left of the option interface element
            array( $this, 'setting_posts' ),                // The name of the function responsible for rendering the option interface
            'soil-settings-page',                           // The page on which this option will be displayed
            'section_soil_general',                         // The name of the section to which this field belongs
            array(                                          // The array of arguments to pass to the callback. In this case, just a description.
                __('Remove the default <code>post</code> post type', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-page', 'soil_hide_posts' );



        add_settings_field(
            'soil_hide_tools',                           // ID used to identify the field throughout the theme
            __('Hide Tools', 'joldnl-soil'),              // The label to the left of the option interface element
            array( $this, 'setting_tools' ),             // The name of the function responsible for rendering the option interface
            'soil-settings-page',                           // The page on which this option will be displayed
            'section_soil_general',                         // The name of the section to which this field belongs
            array(                                          // The array of arguments to pass to the callback. In this case, just a description.
                __('Remove "Tools" from admin menu for <code>client_user</code> and <code>client_admin</code> roles', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-page', 'soil_hide_tools' );



        add_settings_field(
            'soil_hide_comments',                           // ID used to identify the field throughout the theme
            __('Hide Comments', 'joldnl-soil'),              // The label to the left of the option interface element
            array( $this, 'setting_comments' ),             // The name of the function responsible for rendering the option interface
            'soil-settings-page',                           // The page on which this option will be displayed
            'section_soil_general',                         // The name of the section to which this field belongs
            array(                                          // The array of arguments to pass to the callback. In this case, just a description.
                __('Remove "Comments" from admin menu for all user roles', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-page', 'soil_hide_comments' );



        add_settings_field(
            'soil_hide_media',                              // ID used to identify the field throughout the theme
            __('Hide Media', 'joldnl-soil'),                 // The label to the left of the option interface element
            array( $this, 'setting_media' ),                // The name of the function responsible for rendering the option interface
            'soil-settings-page',                           // The page on which this option will be displayed
            'section_soil_general',                         // The name of the section to which this field belongs
            array(                                          // The array of arguments to pass to the callback. In this case, just a description.
                __('Remove "Media" from admin menu for <code>client_user</code> role', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-page', 'soil_hide_media' );



        add_settings_field(
            'soil_dashboard_columns',                       // ID used to identify the field throughout the theme
            __('Dashboard Columns', 'joldnl-soil'),          // The label to the left of the option interface element
            array( $this, 'setting_dashboard_columns' ),    // The name of the function responsible for rendering the option interface
            'soil-settings-page',                           // The page on which this option will be displayed
            'section_soil_general',                         // The name of the section to which this field belongs
            array(                                          // The array of arguments to pass to the callback. In this case, just a description.
                __('Set admin dashboard to single column view', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-page', 'soil_dashboard_columns' );



        add_settings_field(
            'soil_default_roles',                           // ID used to identify the field throughout the theme
            __('Default user roles', 'joldnl-soil'),         // The label to the left of the option interface element
            array( $this, 'setting_default_roles' ),        // The name of the function responsible for rendering the option interface
            'soil-settings-page',                           // The page on which this option will be displayed
            'section_soil_general',                         // The name of the section to which this field belongs
            array(                                          // The array of arguments to pass to the callback. In this case, just a description.
                __('Register <code>client_user</code> and <code>client_admin</code> roles', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-page', 'soil_default_roles' );


    }



    /* ------------------------------------------------------------------------ *
     * Section Callbacks
     * ------------------------------------------------------------------------ */
    function settings_section() {
        echo '<p>Configure Soil with some custom settings.</p>';
    }


    function setting_adminbar($args) {
        $html  = '<input type="checkbox" id="soil_hide_adminbar" name="soil_hide_adminbar" value="true" ' . checked( 'true', get_option('soil_hide_adminbar'), false ) . '/>';
        $html .= '<label for="soil_hide_adminbar"> '  . $args[0] . '</label>';
        echo $html;
    }


    function setting_comments($args) {
        $html  = '<input type="checkbox" id="soil_hide_comments" name="soil_hide_comments" value="true" ' . checked( 'true', get_option('soil_hide_comments'), false ) . '/>';
        $html .= '<label for="soil_hide_comments"> '  . $args[0] . '</label>';
        echo $html;
    }


    function setting_tools($args) {
        $html  = '<input type="checkbox" id="soil_hide_tools" name="soil_hide_tools" value="true" ' . checked( 'true', get_option('soil_hide_tools'), false ) . '/>';
        $html .= '<label for="soil_hide_tools"> '  . $args[0] . '</label>';
        echo $html;
    }


    function setting_posts($args) {
        $html  = '<input type="checkbox" id="soil_hide_posts" name="soil_hide_posts" value="true" ' . checked( 'true', get_option('soil_hide_posts'), false ) . '/>';
        $html .= '<label for="soil_hide_posts"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_media($args) {
        $html  = '<input type="checkbox" id="soil_hide_media" name="soil_hide_media" value="true" ' . checked( 'true', get_option('soil_hide_media'), false ) . '/>';
        $html .= '<label for="soil_hide_media"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_dashboard_columns($args) {
        $html  = '<input type="checkbox" id="soil_dashboard_columns" name="soil_dashboard_columns" value="true" ' . checked( 'true', get_option('soil_dashboard_columns'), false ) . '/>';
        $html .= '<label for="soil_dashboard_columns"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_default_roles($args) {
        $html  = '<input type="checkbox" id="soil_default_roles" name="soil_default_roles" value="true" ' . checked( 'true', get_option('soil_default_roles'), false ) . '/>';
        $html .= '<label for="soil_default_roles"> '  . $args[0] . '</label>';
        echo $html;
    }




    function settings_page_content() {

        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }

        // check if the user have submitted the settings
        // wordpress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            // add_settings_error( 'soil-settings_messages', 'soil-settings_message', __( 'Settings Saved', 'joldnl-soil' ), 'updated' );
        }

        // show error/update messages
        settings_errors( 'soil-settings_messages' );

        // include_once( Core::plugin_path() . 'views/AdminSettings.php' );
        require( Core::plugin_path() . 'views/AdminSettings.php' );

    }




}
