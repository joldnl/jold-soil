<?php

/**
 *
 * Core functions
 *
 * Core functions Class
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Admin Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;


/**
 * Core functions Class
 */
class Functions {



    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

    }



    /**
     *
     * excerpt
     *
     * Customized excerpt loading
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   int       $limit            The number of words to display
     * @param   boolean   $readmore         Add true or false to load read more link
     * @param   string    $input_content    The original excerpt content
     *
     * @return  string                      The customized excerpt content
     *
     */
     public function excerpt( $limit = 100, $readmore = true, $input_content = '' ) {

         if( $input_content ) {

             $excerpt = explode( ' ', $input_content, $limit );

         } else {

             $excerpt = explode( ' ', get_the_content(), $limit );

         }

         if ( count($excerpt) >= $limit ) {

             array_pop( $excerpt );
             $excerpt = implode( " ", $excerpt ) . '...';

         } else {

             $excerpt = implode( " ", $excerpt );

         }

         $excerpt = preg_replace( '/\[.+\]/', '', $excerpt );
         $excerpt = apply_filters( 'the_content', $excerpt );
         $excerpt = str_replace( ']]>', ']]&gt;', $excerpt );
         $excerpt = strip_tags( $excerpt, '' );

         if( $readmore == true ){

             $excerpt .= ' <a href="' . get_permalink() . '" class="read-more">' . __( 'Continued', 'joldnl-soil' ) . '</a>';

         }

         return $excerpt;

     }




    /**
     *
     * get_user_role_by_id
     *
     * Get the user roles from a user_id
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   int       $user_id      The user ID to retrieve the roles for.
     *
     * @return  array                   The user roles as an array
     *
     */
    public static function get_user_roles_by_id( $user_id ) {

        $roles = FALSE;

        if ( !empty( $user_id ) && is_integer( $user_id ) ) {

            $roles = get_userdata( $user_id )->roles;

        }

        if ( $roles ) {

            return $roles[0];

        }

        return;

    }




    /**
     *
     * log
     *
     * Write a log message in the logfile for debugging shit
     *
     * @type	function
     * @date	2018/08/10
     * @author  Jurgen Oldenburg
     *
     * @param   string       $message      The debug/log message to write to the log file
     * @return  n/a
     *
     */
    public static function log( $message ) {

        if ( true === WP_DEBUG ) {

            if ( is_array($message) || is_object($message) ) {

                error_log( print_r($message, true) );

            } else {

                error_log( $message );

            }

        }

    }


    /**
     * Fix a string length, shorten if input is longer than $characters, add spaces if its shorter than the $characters
     * @param  string   $string         The input text
     * @param  int      $characters     Output string length
     * @return string                   The fixed lenght string output
     */
    public static function fixedString( $string = null, $characters = null ) {

        if ( isset($string) && $string !== null && isset($characters) && $characters !== null  ) {

            // Input string length
            $stringLength = strlen($string);


            // Check if input is shorter than max length
            if ( $stringLength < $characters ) {
                $output = str_pad( $string, $characters, ' ');
            }

            // Check if input is longer than max length
            if ( $stringLength >= $characters ) {
                $output = '..' . substr($string, (- $characters + 2) );
            }

            return $output;

        }

        return false;

    }


    static function fileSizeConvert( $bytes ) {

        $bytes = floatval( $bytes );

        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach( $arBytes as $arItem ) {
            if( $bytes >= $arItem["VALUE"] ) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }

        return $result;

    }


}
