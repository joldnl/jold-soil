<?php

/**
 *
 * Whitelabel vendor options class
 *
 * Get details about whitelable settings and vendor preferences
 *
 * @package      Soil
 * @subpackage   Core
 * @category     User
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;


/**
 * Custom Client User role utilities Class
 */
class Vendor {

    const VENDOR_NAME           =     "Dummy Company";
    const VENDOR_EMAIL          =     "noreply@domain.com";
    const VENDOR_PHONE          =     "+31 (0)10 - 123 4567";
    const VENDOR_WEBSITEURL     =     "https://www.dummycompany.nl";
    const VENDOR_SUPPORTURL     =     "https://support.dummycompany.nl";
    const VENDOR_SUPPORTTITLE   =     "Service and support:";
    const VENDOR_SUPPORTINTRO   =     "We handle all our support issues trough our support management system.";

    var $name;
    var $email;
    var $phone;
    var $websiteUrl;
    var $supportUrl;
    var $supportTitle;
    var $supportIntro;


    /**
     *
     * __construct
     * Main class costructor function

     * @return  n/a
     *
     */
    function __construct() {

        // Run WP actions
        add_action( 'admin_init',   array( $this, 'getDetails' )  );

    }


    /**
     * Get the vendor name
     * @return  string  Vendor name
     */
    public function getDetails()
    {
        if ( !$this->getOptionName() ) {
            $this->name = self::VENDOR_NAME;
        } else {
            $this->name = $this->getOptionName();
        }

        if ( !$this->getOptionEmail() ) {
            $this->email = self::VENDOR_EMAIL;
        } else {
            $this->email = $this->getOptionEmail();
        }

        if ( !$this->getOptionPhone() ) {
            $this->phone = self::VENDOR_PHONE;
        } else {
            $this->phone = $this->getOptionPhone();
        }

        if ( !$this->getOptionWebsiteUrl() ) {
            $this->websiteUrl = self::VENDOR_WEBSITEURL;
        } else {
            $this->websiteUrl = $this->getOptionWebsiteUrl();
        }

        if ( !$this->getOptionSupportUrl() ) {
            $this->supportUrl = self::VENDOR_SUPPORTURL;
        } else {
            $this->supportUrl = $this->getOptionSupportUrl();
        }

        if ( !$this->getOptionSupportTitle() ) {
            $this->supportTitle = self::VENDOR_SUPPORTTITLE;
        } else {
            $this->supportTitle = $this->getOptionSupportTitle();
        }

        if ( !$this->getOptionSupportIntro() ) {
            $this->supportIntro = self::VENDOR_SUPPORTINTRO;
        } else {
            $this->supportIntro = $this->getOptionSupportIntro();
        }

        return $this;

    }



    /**
     * Get the vendor name
     * @return  string  Vendor name
     */
    public function getOptionName()
    {
        return get_option('soil_vendor_name');
    }


    /**
     * Get the vendor email wp option
     * @return  string  Vendor email wp option
     */
    public function getOptionEmail()
    {
        return get_option('soil_vendor_email');
    }

    /**
     * Get the vendor phone wp option
     * @return  string  Vendor phone wp option
     */
    public function getOptionPhone()
    {
        return get_option('soil_vendor_phone');
    }

    /**
     * Get the vendor website url wp option
     * @return  string  Vendor website url wp option
     */
    public function getOptionWebsiteUrl()
    {
        return get_option('soil_vendor_website_url');
    }

    /**
     * Get the stripped vendor website url wp option
     * @return  string  Clean vendor website url wp option
     */
    public function getOptionWebsiteUrlClean()
    {
        return $this->cleanUrl( get_option('soil_vendor_website_url') );
    }

    /**
     * Get the vendor support url wp option
     * @return  string  Vendor support url wp option
     */
    public function getOptionSupportTitle()
    {
        return get_option('soil_vendor_support_title');
    }

    /**
     * Get the vendor support url wp option
     * @return  string  Vendor support url wp option
     */
    public function getOptionSupportIntro()
    {
        return get_option('soil_vendor_support_intro');
    }

    /**
     * Get the vendor support url wp option
     * @return  string  Vendor support url wp option
     */
    public function getOptionSupportUrl()
    {
        return get_option('soil_vendor_support_url');
    }

    /**
    * Get the stripped vendor website url wp option
    * @return  string  Clean vendor website url wp option
    */
    public function getOptionSupportUrlClean()
    {
        return $this->cleanUrl( $this->websiteUrl );
    }


    /**
     * Get the vendor support url wp option
     * @return  string  Vendor support url wp option
     */
    private function cleanUrl( $url )
    {

        if ( $url ) {

            $urlClean = str_replace( 'https://', '', $url );
            $urlClean = str_replace( 'http://', '', $urlClean );
            $urlClean = str_replace( 'www.', '', $urlClean );

            return $urlClean;

        }

        return false;

    }





}
