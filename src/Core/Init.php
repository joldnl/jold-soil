<?php

/**
 *
 * Admin Init and cleanup class
 *
 * Main admin cleanup and dashboard class and functions enhancing the default wp-admin.
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;


/**
 * Admin init functions
 */
class Init {



    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        // add_action( 'wp_dashboard_setup',   array( $this, 'dashboard_widget_pms72_register' ) );

    }

}
