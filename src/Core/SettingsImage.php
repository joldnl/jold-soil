<?php

/**
 *
 * Soil settings configuration class
 *
 * Yoast settings class
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Settings
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;

use Soil\Core;
use Soil\Core\Functions;
use Soil\Core\ImageOptimization;


/**
 * Admin init functions
 */
class SettingsImage {


    public $settings;


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        add_action( 'admin_init',   [ $this, 'settings_init' ] );

    }



    /**
     *
     * settings_init
     *
     * Initiate and register settings sections and fields
     *
     * @return  n/a
     *
     */
    function settings_init() {


        /**
         * Soil Yoast Cleanup Section
         */
        add_settings_section(
            'section_soil_image',                                // $id          ID used to identify this section and with which to register options
            __('Image Optimization', 'joldnl-soil'),             // $title       Title to be displayed on the administration page
            [$this, 'settings_section'],                         // $callback    Callback used to render the description of the section
            'soil-settings-image-page'                           // $page        Page on which to add this section of options
        );


        /**
         * Enable Optimization in general
         */
        add_settings_field(
            'soil_image_enable_optimization',                    // $id          ID used to identify the field throughout the theme
            __('Activate Optimization', 'joldnl-soil'),          // $title       The label to the left of the option interface element
            [$this, 'setting_image_enable_optimization'],        // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-image-page',                          // $page        The page on which this option will be displayed
            'section_soil_image',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Check this to use compressed images.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-image-page', 'setting_image_enable_optimization' );


        /**
         * ShortPixel Key
         */
        add_settings_field(
            'soil_image_shortpixel_key',                         // $id          ID used to identify the field throughout the theme
            __('ShortPixel Key', 'joldnl-soil'),                 // $title       The label to the left of the option interface element
            [$this, 'setting_image_shortpixel_key'],             // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-image-page',                          // $page        The page on which this option will be displayed
            'section_soil_image',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Enter your <a href="https://shortpixel.com/show-api-key" target="_blank">ShortPixel</a> API Key', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-image-page', 'setting_image_shortpixel_key' );


        /**
         * Create compressed image files
         */
        add_settings_field(
            'soil_image_enable_compression',                     // $id          ID used to identify the field throughout the theme
            __('Create compressed images', 'joldnl-soil'),       // $title       The label to the left of the option interface element
            [$this, 'setting_image_enable_compression'],         // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-image-page',                          // $page        The page on which this option will be displayed
            'section_soil_image',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Create compressed images (slows down website when generating new resized images).', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-image-page', 'setting_image_enable_compression' );


        /**
         * Create WebP image files
         */
        add_settings_field(
            'soil_image_enable_webp',                            // $id          ID used to identify the field throughout the theme
            __('Create WebP files', 'joldnl-soil'),              // $title       The label to the left of the option interface element
            [$this, 'setting_image_enable_webp'],                // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-image-page',                          // $page        The page on which this option will be displayed
            'section_soil_image',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Create WebP images (slows down website when generating new resized images).', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-image-page', 'setting_image_enable_webp' );


        /**
         * Enable Compressed images
         */
        add_settings_field(
            'soil_image_show_compressed',                        // $id          ID used to identify the field throughout the theme
            __('Serve Compressed Images', 'joldnl-soil'),        // $title       The label to the left of the option interface element
            [$this, 'setting_image_show_compressed'],            // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-image-page',                          // $page        The page on which this option will be displayed
            'section_soil_image',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Serve compressed images in your theme.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-image-page', 'setting_image_show_compressed' );


        /**
         * Enable WebP
         */
        add_settings_field(
            'soil_image_show_webp',                              // $id          ID used to identify the field throughout the theme
            __('Serve WebP Images', 'joldnl-soil'),              // $title       The label to the left of the option interface element
            [$this, 'setting_image_show_webp'],                  // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-image-page',                          // $page        The page on which this option will be displayed
            'section_soil_image',                                // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Serve WebP images in your theme', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-image-page', 'setting_image_show_webp' );


        /**
         * Soil Yoast Cleanup Section
         */
        add_settings_section(
            'section_soil_detail',                                // $id          ID used to identify this section and with which to register options
            __('Optimization summary', 'joldnl-soil'),             // $title       Title to be displayed on the administration page
            [$this, 'settings_details_table'],                         // $callback    Callback used to render the description of the section
            'soil-settings-image-page'                           // $page        Page on which to add this section of options
        );

    }


    public function disable_field() {

        $enabled  = get_option('setting_image_enable_optimization');
        $disabled = ' disabled="disabled"';

        if ( !$enabled ) {
            return $disabled;
        }

    }


    /* ------------------------------------------------------------------------ *
     * Section Callbacks
     * ------------------------------------------------------------------------ */
    function settings_section() {
        echo '<p><strong>Enable and configure image compression and WebP creation.</strong></p>';
        echo '<p>Soil only saves resized images that are actually used, generating them on page load, if the dont exist Yet. These settings allow you to generate optimized versions of the original saved uploades, and even get WebP files.</p>';
    }

    function settings_details_table() {

        $optim = new ImageOptimization;
        $summary = $optim->assetsSummary();

?>
<table class="wp-list-table widefat fixed striped posts">
    <tr>
        <th style="width: 200px;">Cache Folders</th>
        <th style="width: 200px;">Total Assets</th>
        <th style="width: 200px;">Total jpg files</th>
        <th style="width: 200px;">Total webp files</th>
    </tr>
    <tr>
        <td><?php echo $summary['cacheFolders']; ?></td>
        <td><?php echo $summary['assetsTotal']; ?></td>
        <td><?php echo $summary['assetsJpg']; ?></td>
        <td><?php echo $summary['assetsWebp']; ?></td>
    </tr>
</table>
<?php

    }

    function setting_image_enable_optimization($args) {
        $html  = '<input type="checkbox" id="setting_image_enable_optimization" name="setting_image_enable_optimization" value="true" ' . checked( 'true', get_option('setting_image_enable_optimization'), false ) . '/>';
        $html .= '<label for="setting_image_enable_optimization"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_image_shortpixel_key($args) {
        $html = '<input type="text" id="setting_image_shortpixel_key" name="setting_image_shortpixel_key" value="' . get_option('setting_image_shortpixel_key') . '" class="regular-text" ' . $this->disable_field() . '>';
        $html .= '<p><label for="setting_image_shortpixel_key"> '  . $args[0] . '</label></p>';
        echo $html;
    }

    function setting_image_show_compressed($args) {
        $html  = '<input type="checkbox" id="setting_image_show_compressed" name="setting_image_show_compressed" value="true" ' . checked( 'true', get_option('setting_image_show_compressed'), false ) . $this->disable_field() . '/>';
        $html .= '<label for="setting_image_show_compressed"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_image_show_webp($args) {
        $html  = '<input type="checkbox" id="setting_image_show_webp" name="setting_image_show_webp" value="true" ' . checked( 'true', get_option('setting_image_show_webp'), false ) . $this->disable_field() . '/>';
        $html .= '<label for="setting_image_show_webp"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_image_enable_compression($args) {
        $html  = '<input type="checkbox" id="setting_image_enable_compression" name="setting_image_enable_compression" value="true" ' . checked( 'true', get_option('setting_image_enable_compression'), false ) . $this->disable_field() . '/>';
        $html .= '<label for="setting_image_enable_compression"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_image_enable_webp($args) {
        $html  = '<input type="checkbox" id="setting_image_enable_webp" name="setting_image_enable_webp" value="true" ' . checked( 'true', get_option('setting_image_enable_webp'), false ) . $this->disable_field() . '/>';
        $html .= '<label for="setting_image_enable_webp"> '  . $args[0] . '</label>';
        echo $html;
    }


    function settings_page_content() {

        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }

        // check if the user have submitted the settings
        // wordpress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            // add_settings_error( 'soil-settings_messages', 'soil-settings_message', __( 'Settings Saved', 'joldnl-soil' ), 'updated' );
        }

        // show error/update messages
        settings_errors( 'soil-settings_messages' );

        // include_once( Core::plugin_path() . 'views/AdminSettings.php' );
        require( Core::plugin_path() . 'views/AdminSettings.php' );

    }

}
