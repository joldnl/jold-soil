<?php

/**
 *
 * Client User role functions
 *
 * Create custom user roles client_user and client_admin
 *
 * @package      Soil
 * @subpackage   Core
 * @category     User
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;


/**
 * Custom Client User role utilities Class
 */
class User {


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        // Run WP actions
        add_action( 'admin_init', array( $this, 'register_client_roles' )  );

        add_filter( 'editable_roles', array( $this, 'client_hide_default_roles' ) );


    }



    /**
     *
     * register_client_roles
     *
     * Check if the client roles exists, and if not, register them!
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function register_client_roles( $disable ) {

        // Check if the 'disable admin roles' setting from the admin is set
        $register = get_option('soil_default_roles');

        // Register client roldes
        if ( $register == true ) {
            if ( ! $this->role_exists( 'client_user' ) ) {
                $this->register_client_user();
            }
            if ( ! $this->role_exists( 'client_admin' ) ) {
                $this->register_client_admin();
            }
        }

        // Remove client roldes
        elseif ( $register == false ) {
            if ( $this->role_exists( 'client_user' ) ) {
                $this->remove_role( 'client_user' );
            }
            if ( $this->role_exists( 'client_admin' ) ) {
                $this->remove_role( 'client_admin' );
            }
        }

    }



	/**
     *
     * client_roles
     *
     * The list of custom user roles
     *
     * @type	function
     * @date	2017/03/15
     * @author  Jurgen Oldenburg
     * @since	1.0.10
     *
     * @return  n/a
     *
     */
    public static function client_roles() {

        $roles = [
            'client_user',
            'client_admin',
        ];

        return $roles;

    }



    /**
     *
     * role_exists
     *
     * Check if a user role exists
     *
     * @type	function
     * @date	2017/03/15
     * @author  Jurgen Oldenburg
     * @since	1.0.10
     *
     * @param   string      $role   The slug of the user role to check
     *
     * @return  boolean             Return true if the user role exists
     *
     */
    public function role_exists( $role ) {

        if( ! empty( $role ) ) {

            return $GLOBALS['wp_roles']->is_role( $role );

        }

        return false;

    }



	/**
     *
     * current_user
     *
     * Get the current user object
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
	function current_user() {

        $user = wp_get_current_user();

        if( isset( $user ) && !empty( $user ) && is_object( $user ) ) {

            return wp_get_current_user();

        }

        return false;

	}



    /**
     *
     * user_has_role
     *
     * Check if a user has a role from the roles array in it's WP_User object
     *
     * @type    function
     * @date    2017/08/20
     * @since   1.0.16
     * @author  Jurgen Oldenburg
     *
     * @param   object  $user_object    WP_User object of a wp user
     * @param   string  $role           The role to check for
     *
     * @return  boolean                 Return true if $role exists in the user object
     *
     */
    public function user_has_role( $user_object, $role ) {

        if ( is_object($user_object) && get_class($user_object) == 'WP_User' ) {

            $user_roles = $user_object->roles;

            // Check if a user is passed
            if ( isset($user_roles) && !empty($user_roles) && is_array($user_roles) ) {

                // Check if $role exists in $user_roles from the $user object
                $has_role = array_search($role, $user_roles);

                if ( $has_role !== false && is_int($has_role) ) {
                    return true;
                }

            }

        }

        return false;

    }



    /**
     *
     * is_client_role
     *
     * Check if the current logged in user is a client user.
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public static function is_client_role() {

        if ( is_user_logged_in() ) {

            $soil_user  = new User();
            $user       = $soil_user->current_user();
            $roles      = $user->roles[0];

            if ( in_array( $roles, self::client_roles() )  ) {

                return true;

            }

            return false;
        }

    }



    /**
     *
     * is_admin
     *
     * Check if the current logged in user has 'administrator' user role.
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public static function is_admin() {

        if ( current_user_can( 'administrator' )  ) {
            return true;
        }

        return false;

    }



    /**
     *
     * is_client_admin
     *
     * Check if the current logged in user has 'client_admin' user role.
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  boolean     True if the user has role 'client_admin'.
     *
     */
    public static function is_client_admin() {

        $user_object = wp_get_current_user();

        if ( in_array( 'client_admin',  $user_object->roles ) ) {
            return true;
        }

        return false;

    }



    /**
     *
     * is_client_user
     *
     * Check if the current logged in user has 'client_user' user role.
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  boolean     True if the user has role 'client_user'.
     *
     */
    public static function is_client_user() {

        $user_object = wp_get_current_user();

        if ( in_array( 'client_user',  $user_object->roles ) ) {
            return true;
        }

        return false;

    }



    /**
     *
     * register_role
     *
     * Register a new user role
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @param   string    $slug   The role slug
     * @param   string    $name   The role name
     *
     * @return  boolean           Returns true if role does not exist and has been registered.
     *
     */
    public function register_role( $slug, $name ) {


        if ( !empty( $slug ) && is_string( $slug ) ) {

            if ( !empty( $name ) && is_string( $name ) ) {

                $role = add_role(
                    $slug,
                    __( $name ),
                    array(
                        'read' => true,  // Allow reading (login into wp-admin)
                    )
                );


            }

        }

        if ( null !== $role ) {

            add_action( 'admin_notices', array( $this, 'register_role_admin_notice') );

            return $role;

        }

        return false;

    }



    /**
     *
     * remove_role
     *
     * Register a new user role
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @param   string    $slug   The role slug
     * @param   string    $name   The role name
     *
     * @return  boolean           Returns true if role does not exist and has been registered.
     *
     */
    public function remove_role( $slug ) {

        if ( !empty( $slug ) && is_string( $slug ) ) {

            remove_role( $slug );

            add_action( 'admin_notices', array( $this, 'remove_role_admin_notice') );

        }

        return false;

    }



    /**
     *
     * admin_notice_register_roles
     *
     * Display an admin notice for registring custom user roles
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  string      The admin notice html
     *
     */
    public function register_role_admin_notice() {

        echo '<div class="notice notice-success is-dismissible"><p>' . __( 'Default Soil user roles (<code>client_user</code> &amp; <code>client_admin</code>) have been <strong><em>created!</em></strong>', 'joldnl-soil' ) . '</p></div>';

    }




    /**
     *
     * remove_role_admin_notice
     *
     * Display an admin notice deleting custom user roles
     *
     * @type	function
     * @date	2017/03/16
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  string      The admin notice html
     *
     */
    public function remove_role_admin_notice() {

        echo '<div class="notice notice-success is-dismissible"><p>' . __( 'Default Soil user roles (<code>client_user</code> &amp; <code>client_admin</code>) have been <strong><em>removed!</em></strong>', 'joldnl-soil' ) . '</p></div>';

    }




    /**
     *
     * register_client_admin
     *
     * Register 'client_admin' role and add capabilities. This role is based on the WordPress 'editor' role.
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function register_client_admin() {

        $slug   = 'client_admin';
        $title  = __( 'Client Admin', 'joldnl-soil' );

        // Register the client user role
        $this->register_role( $slug, $title );

        // Get the role object for manipulation (add capabilities)
        $role   = get_role( $slug );

        // Set default WordPress capabilities from 'editor' user role
        $role->add_cap( 'manage_categories' );
        $role->add_cap( 'manage_links' );
        $role->add_cap( 'moderate_comments' );
        $role->add_cap( 'read' );
        $role->add_cap( 'unfiltered_html' );
        $role->add_cap( 'upload_files' );

        // Posts
        $role->add_cap( 'delete_others_posts' );
        $role->add_cap( 'delete_posts' );
        $role->add_cap( 'delete_private_posts' );
        $role->add_cap( 'delete_published_posts' );
        $role->add_cap( 'edit_others_posts' );
        $role->add_cap( 'edit_posts' );
        $role->add_cap( 'edit_private_posts' );
        $role->add_cap( 'edit_published_posts' );
        $role->add_cap( 'publish_posts' );
        $role->add_cap( 'read_private_posts' );

        // Articles
        $role->add_cap( 'delete_others_articles' );
        $role->add_cap( 'delete_articles' );
        $role->add_cap( 'delete_private_articles' );
        $role->add_cap( 'delete_published_articles' );
        $role->add_cap( 'edit_others_articles' );
        $role->add_cap( 'edit_articles' );
        $role->add_cap( 'edit_private_articles' );
        $role->add_cap( 'edit_published_articles' );
        $role->add_cap( 'publish_articles' );
        $role->add_cap( 'read_private_articles' );

        // Pages
        $role->add_cap( 'delete_others_pages' );
        $role->add_cap( 'delete_pages' );
        $role->add_cap( 'delete_private_pages' );
        $role->add_cap( 'delete_published_pages' );
        $role->add_cap( 'edit_others_pages' );
        $role->add_cap( 'edit_pages' );
        $role->add_cap( 'edit_private_pages' );
        $role->add_cap( 'edit_published_pages' );
        $role->add_cap( 'publish_pages' );
        $role->add_cap( 'read_private_pages' );


        // Add custom extra capabilities
        $role->add_cap( 'edit_users' );
        $role->add_cap( 'create_users' );
        $role->add_cap( 'list_users' );
        $role->add_cap( 'promote_users' );
        $role->add_cap( 'edit_theme_options' );

        // Administrator user rights:
        // $role->add_cap( 'delete_users' );
        // $role->add_cap( 'remove_users' );

        // Gravityforms caps:
        $role->add_cap( 'gravityforms_create_form' );
        $role->add_cap( 'gravityforms_delete_entries' );
        $role->add_cap( 'gravityforms_delete_forms' );
        $role->add_cap( 'gravityforms_edit_entries' );
        $role->add_cap( 'gravityforms_edit_entry_notes' );
        $role->add_cap( 'gravityforms_edit_forms' );
        // $role->add_cap( 'gravityforms_edit_settings' );
        $role->add_cap( 'gravityforms_export_entries' );
        $role->add_cap( 'gravityforms_preview_forms' );
        // $role->add_cap( 'gravityforms_system_status' );
        // $role->add_cap( 'gravityforms_uninstall' );
        $role->add_cap( 'gravityforms_view_addons' );
        $role->add_cap( 'gravityforms_view_entries' );
        $role->add_cap( 'gravityforms_view_entry_notes' );
        // $role->add_cap( 'gravityforms_view_settings' );
        // $role->add_cap( 'gravityforms_view_updates' );

    }



    /**
     *
     * register_client_user
     *
     * Register 'client_user' role and add capabilities. This role is based on the WordPress 'author' role.
     *
     * @type	function
     * @date	2017/03/15
     * @since	1.0.10
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function register_client_user() {

        $slug   = 'client_user';
        $title  = __( 'Client User', 'joldnl-soil' );

        // Register the client user role
        $this->register_role( $slug, $title );

        // Get the role object for manipulation (add capabilities)
        $role   = get_role( $slug );

        // Set default WordPress capabilities from 'editor' user role
        $role->add_cap( 'manage_categories' );
        $role->add_cap( 'manage_links' );
        $role->add_cap( 'moderate_comments' );
        $role->add_cap( 'read' );
        $role->add_cap( 'unfiltered_html' );
        $role->add_cap( 'upload_files' );

        // Posts
        $role->add_cap( 'delete_others_posts' );
        $role->add_cap( 'delete_posts' );
        // $role->add_cap( 'delete_private_posts' );
        $role->add_cap( 'delete_published_posts' );
        $role->add_cap( 'edit_others_posts' );
        $role->add_cap( 'edit_posts' );
        // $role->add_cap( 'edit_private_posts' );
        $role->add_cap( 'edit_published_posts' );
        $role->add_cap( 'publish_posts' );
        // $role->add_cap( 'read_private_posts' );

        // Articles
        $role->add_cap( 'delete_others_articles' );
        $role->add_cap( 'delete_articles' );
        // $role->add_cap( 'delete_private_articles' );
        $role->add_cap( 'delete_published_articles' );
        $role->add_cap( 'edit_others_articles' );
        $role->add_cap( 'edit_articles' );
        // $role->add_cap( 'edit_private_articles' );
        $role->add_cap( 'edit_published_articles' );
        $role->add_cap( 'publish_articles' );
        // $role->add_cap( 'read_private_articles' );

        // Pages
        $role->add_cap( 'delete_others_pages' );
        $role->add_cap( 'delete_pages' );
        // $role->add_cap( 'delete_private_pages' );
        $role->add_cap( 'delete_published_pages' );
        $role->add_cap( 'edit_others_pages' );
        $role->add_cap( 'edit_pages' );
        // $role->add_cap( 'edit_private_pages' );
        $role->add_cap( 'edit_published_pages' );
        $role->add_cap( 'publish_pages' );
        // $role->add_cap( 'read_private_pages' );

        // Add custom extra capabilities
        // $role->add_cap( 'edit_users' );
        // $role->add_cap( 'create_users' );
        // $role->add_cap( 'list_users' );
        // $role->add_cap( 'promote_users' );
        // $role->add_cap( 'edit_theme_options' );

        // Administrator user rights:
        // $role->add_cap( 'delete_users' );
        // $role->add_cap( 'remove_users' );

        // Gravityforms caps:
        $role->add_cap( 'gravityforms_create_form' );
        $role->add_cap( 'gravityforms_delete_entries' );
        $role->add_cap( 'gravityforms_delete_forms' );
        $role->add_cap( 'gravityforms_edit_entries' );
        $role->add_cap( 'gravityforms_edit_entry_notes' );
        $role->add_cap( 'gravityforms_edit_forms' );
        // $role->add_cap( 'gravityforms_edit_settings' );
        $role->add_cap( 'gravityforms_export_entries' );
        $role->add_cap( 'gravityforms_preview_forms' );
        // $role->add_cap( 'gravityforms_system_status' );
        // $role->add_cap( 'gravityforms_uninstall' );
        $role->add_cap( 'gravityforms_view_addons' );
        $role->add_cap( 'gravityforms_view_entries' );
        $role->add_cap( 'gravityforms_view_entry_notes' );
        // $role->add_cap( 'gravityforms_view_settings' );
        // $role->add_cap( 'gravityforms_view_updates' );

    }



    /**
     *
     * client_hide_default_roles
     *
     * Hide default roles for soil users
     * The default wp users are hidden in the user role dropdown on edit profile page
     *
     * @type	function
     * @date	2019/11/19
     * @since	1.3.0
     * @author  Jurgen Oldenburg
     *
     * @param     array    $all_roles     All registered user roles
     *
     * @return    array                   Filtered user roles to only client_admin and client_user
     *
     */
    public function client_hide_default_roles($all_roles) {

        $unset = [
            'administrator',
            'editor',
            'author',
            'contributor',
            'subscriber',
            // 'customer',
            // 'shop_manager',
        ];


        if ( self::is_client_role() ) {
            foreach ( $all_roles as $name => $role ) {
                if (in_array($name, $unset)) {
                    unset($all_roles[$name]);
                }
            }
        }

        // var_dump( $all_roles );
        // exit;

        return $all_roles;

    }



}
