<?php

/**
 *
 * Soil settings configuration class
 *
 * Main configuration class
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Core;

use Soil\Core;
use Soil\Core\Functions;


/**
 * Admin init functions
 */
class SettingsVendor {


    public $settings;
    public $options_page;


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {


        /**
         * Populate the WDCD Admin Settings
         * @var array
         */
        $this->options_page = [
            'id'            => 'soil-settings-vendor-page',
            'page_title'    => __( 'Soil White-label', 'joldnl-soil' ),
            'menu_title'    => __( 'Soil White-label', 'joldnl-soil' ),
        ];


        add_action( 'admin_init',       array( $this, 'settings_init' ) );

    }



    /**
     *
     * settings_init
     *
     * Initiate and register settings sections and fields
     *
     * @return  n/a
     *
     */
    function settings_init() {

        /**
         * Soil White-label settings Section
         */
        add_settings_section(
            'section_soil_vendor',                              // $id          ID used to identify this section and with which to register options
            __('Soil White-label settings', 'joldnl-soil'),     // $title       Title to be displayed on the administration page
            [$this, 'settings_section'],                        // $callback    Callback used to render the description of the section
            'soil-settings-vendor-page'                         // $page        Page on which to add this section of options
        );

        /**
         * Vendor name
         */
        add_settings_field(
            'soil_vendor_name',                                 // $id          ID used to identify the field throughout the theme
            __('Vendor name', 'joldnl-soil'),                   // $title       The label to the left of the option interface element
            [$this, 'setting_vendor_name'],                     // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                        // $page        The page on which this option will be displayed
            'section_soil_vendor',                              // $section     The name of the section to which this field belongs
            array(                                              // $args        The array of arguments to pass to the callback
                __('Company name of the white label party.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_vendor_name' );

        /**
         * Vendor email
         */
        add_settings_field(
            'soil_vendor_email',                                // $id          ID used to identify the field throughout the theme
            __('Vendor email', 'joldnl-soil'),                  // $title       The label to the left of the option interface element
            [$this, 'setting_vendor_email'],                    // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                        // $page        The page on which this option will be displayed
            'section_soil_vendor',                              // $section     The name of the section to which this field belongs
            array(                                              // $args        The array of arguments to pass to the callback
                __('Company email of the white label party.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_vendor_email' );

        /**
         * Vendor phone
         */
        add_settings_field(
            'soil_vendor_phone',                                // $id          ID used to identify the field throughout the theme
            __('Vendor phone', 'joldnl-soil'),                  // $title       The label to the left of the option interface element
            [$this, 'setting_vendor_phone'],                    // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                        // $page        The page on which this option will be displayed
            'section_soil_vendor',                              // $section     The name of the section to which this field belongs
            array(                                              // $args        The array of arguments to pass to the callback
                __('Company phone of the white label party.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_vendor_phone' );

        /**
         * Vendor website url
         */
        add_settings_field(
            'soil_vendor_website_url',                           // $id          ID used to identify the field throughout the theme
            __('Vendor website url', 'joldnl-soil'),             // $title       The label to the left of the option interface element
            [$this, 'setting_vendor_website_url'],               // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                         // $page        The page on which this option will be displayed
            'section_soil_vendor',                               // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Company website_url of the white label party.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_vendor_website_url' );

        /**
         * Vendor support url
         */
        add_settings_field(
            'soil_vendor_support_url',                           // $id          ID used to identify the field throughout the theme
            __('Vendor support url', 'joldnl-soil'),             // $title       The label to the left of the option interface element
            [$this, 'setting_vendor_support_url'],               // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                         // $page        The page on which this option will be displayed
            'section_soil_vendor',                               // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Company support_url of the white label party.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_vendor_support_url' );


        /**
         * Support title
         */
        add_settings_field(
            'soil_vendor_support_title',                         // $id          ID used to identify the field throughout the theme
            __('Vendor support title', 'joldnl-soil'),           // $title       The label to the left of the option interface element
            [$this, 'setting_vendor_support_title'],             // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                         // $page        The page on which this option will be displayed
            'section_soil_vendor',                               // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Company support_title of the white label party.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_vendor_support_title' );

        /**
         * Support intro
         */
        add_settings_field(
            'soil_vendor_support_intro',                         // $id          ID used to identify the field throughout the theme
            __('Vendor support intro', 'joldnl-soil'),           // $title       The label to the left of the option interface element
            [$this, 'setting_vendor_support_intro'],             // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                         // $page        The page on which this option will be displayed
            'section_soil_vendor',                               // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Company support_intro of the white label party.', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_vendor_support_intro' );





        /**
         * Admin Footer Section
         */
        add_settings_section(
            'soil_vendor_title_footer',                          // $id          ID used to identify this section and with which to register options
            __('Admin Footer', 'joldnl-soil'),                   // $title       Title to be displayed on the administration page
            [$this, 'desc_footer_text'],                         // $callback    Callback used to render the description of the section
            'soil-settings-vendor-page'                          // $page        Page on which to add this section of options
        );

        /**
         * Overwrite admin-footer html
         */
        add_settings_field(
            'soil_admin_footer_html',                            // $id          ID used to identify the field throughout the theme
            __('Overwrite admin-footer html', 'joldnl-soil'),    // $title       The label to the left of the option interface element
            [$this, 'setting_admin_footer_html'],                // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                         // $page        The page on which this option will be displayed
            'soil_vendor_title_footer',                          // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Overwrite the output of the admin footer html with your custom html', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_admin_footer_html' );

        /**
         * Disavle admin-footer
         */
        add_settings_field(
            'soil_admin_footer_disable',                         // $id          ID used to identify the field throughout the theme
            __('Disable admin-footer text', 'joldnl-soil'),      // $title       The label to the left of the option interface element
            [$this, 'setting_admin_footer_disable'],             // $callback    The name of the function responsible for rendering the option interface
            'soil-settings-vendor-page',                         // $page        The page on which this option will be displayed
            'soil_vendor_title_footer',                          // $section     The name of the section to which this field belongs
            array(                                               // $args        The array of arguments to pass to the callback
                __('Hide the footer text in admin entirely', 'joldnl-soil')
            )
        );
        register_setting( 'soil-settings-vendor-page', 'soil_admin_footer_disable' );

    }



    /* ------------------------------------------------------------------------ *
     * Section Callbacks
     * ------------------------------------------------------------------------ */
    function settings_section() {
        echo '<p>These settins are used on the dashboard welcoe widget.</p>';
    }

    function setting_vendor_name($args) {
        $html = '<input type="text" id="soil_vendor_name" name="soil_vendor_name" value="' . get_option('soil_vendor_name') . '" class="regular-text">';
        echo $html;
    }

    function setting_vendor_email($args) {
        $html = '<input type="email" id="soil_vendor_email" name="soil_vendor_email" value="' . get_option('soil_vendor_email') . '" class="regular-text">';
        echo $html;
    }

    function setting_vendor_phone($args) {
        $html = '<input type="tel" id="soil_vendor_phone" name="soil_vendor_phone" value="' . get_option('soil_vendor_phone') . '" class="regular-text">';
        echo $html;
    }

    function setting_vendor_website_url($args) {
        $html = '<input type="url" id="soil_vendor_website_url" name="soil_vendor_website_url" value="' . get_option('soil_vendor_website_url') . '" class="regular-text">';
        echo $html;
    }

    function setting_vendor_support_url($args) {
        $html = '<input type="url" id="soil_vendor_support_url" name="soil_vendor_support_url" value="' . get_option('soil_vendor_support_url') . '" class="regular-text">';
        echo $html;
    }
    function setting_vendor_support_title($args) {
        $html = '<input type="text" id="soil_vendor_support_title" name="soil_vendor_support_title" value="' . get_option('soil_vendor_support_title') . '" class="regular-text">';
        echo $html;
    }
    function setting_vendor_support_intro($args) {
        $html = '<input type="text" id="soil_vendor_support_intro" name="soil_vendor_support_intro" value="' . get_option('soil_vendor_support_intro') . '" class="regular-text">';
        echo $html;
    }



    function desc_footer_text() {
        echo '<p>Hide or change the admin footer contents</p>';
    }

    function setting_admin_footer_disable($args) {
        $html  = '<input type="checkbox" id="soil_admin_footer_disable" name="soil_admin_footer_disable" value="true" ' . checked( 'true', get_option('soil_admin_footer_disable'), false ) . '/>';
        $html .= '<label for="soil_admin_footer_disable"> '  . $args[0] . '</label>';
        echo $html;
    }

    function setting_admin_footer_html($args) {
        $html  = '<p><textarea id="soil_admin_footer_html" name="soil_admin_footer_html" class="code" rows="2" style="width: 350px;">' . get_option('soil_admin_footer_html') . '</textarea><br />';
        $html .= '<label for="soil_admin_footer_html"> '  . $args[0] . '</label></p>';
        // $html  = '<input type="checkbox" id="soil_admin_footer_html" name="soil_admin_footer_text" value="true" ' . checked( 'true', get_option('soil_admin_footer_text'), false ) . '/>';
        echo $html;
    }




    function settings_page_content() {

        // check user capabilities
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }

        // check if the user have submitted the settings
        // wordpress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            // add_settings_error( 'soil-settings_messages', 'soil-settings_message', __( 'Settings Saved', 'joldnl-soil' ), 'updated' );
        }

        // show error/update messages
        settings_errors( 'soil-settings_messages' );

        // include_once( Core::plugin_path() . 'views/AdminSettings.php' );
        require( Core::plugin_path() . 'views/AdminSettings.php' );

    }




}
