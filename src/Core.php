<?php

/**
 *
 * Core functions
 *
 * Core functions Class
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Admin Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil;

use Soil\Core\Vendor;
use Soil\Core\ImageOptimization;
use Soil\Core\Functions;

/**
 * Core functions Class
 */
class Core {


    // public $plugin_path = "sdfd";


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        // Run WordPress actions
        add_action( 'init',                                         array( $this, 'vendor' ) );

        // Run WordPress filters
        add_filter( "plugin_action_links_joldnl-soil/soil.php",     array( $this, 'settings_link' ) );

    }


    /**
     *
     * plugin_path
     *
     * Return the path of this plugin
     *
     * @type	function
     * @date	2017/03/09
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public static function plugin_path() {

        // $path = str_replace( '/src/', '/', plugin_dir_path( __FILE__  ) );
        // $path = str_replace( '/src/', '/', plugin_dir_path( __FILE__  ) );
        // $path = str_replace( '\\', '/', plugin_dir_path( __FILE__  ) );

        $path = str_replace( "\\", '/', plugin_dir_path( __FILE__  ) );
        $path = str_replace( '/src/', '/', $path );

        return $path;

    }




    /**
     *
     * plugin_name
     *
     * Return the sanitized name of this plugin
     *
     * @type	function
     * @date	2017/04/19
     * @since	1.0.16
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function plugin_name() {

        $name = 'joldnl-soil';

        return $name;

    }



    /**
     *
     * settings_path
     *
     * Return the path of the settings page for this plugin
     *
     * @type	function
     * @date	2017/04/19
     * @since	1.0.16
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function settings_path() {

        $settings = 'soil-settings-page';

        return $settings;

    }


    /**
     *
     * settings_link
     *
     * Add a settings link to the plugins list.
     *
     * @type	function
     * @date	2017/04/19
     * @since	1.0.16
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function settings_link( $links ) {

        $settings_link = '<a href="options-general.php?page=' . $this->settings_path() . '">' . __( 'Settings', 'joldnl-soil' ) . '</a>';

        array_push( $links, $settings_link );

      	return $links;

    }



    /**
     *
     * vendor
     *
     * Return the PMS72 contact details
     *
     * @type	function
     * @date	2017/03/09
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  array   $details    The contact details
     *
     */
    public static function vendor() {

        $vendor = new Vendor();

        return $vendor->getDetails();

    }



    /**
     *
     * excerpt
     *
     * Customized excerpt loading
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   int       $limit            The number of words to display
     * @param   boolean   $readmore         Add true or false to load read more link
     * @param   string    $input_content    The original excerpt content
     *
     * @return  string                      The customized excerpt content
     *
     * @TODO:   Make css class and link text available as parameters
     *
     */
    public function excerpt( $limit = 100, $readmore = true, $input_content = '' ) {

        if( $input_content ) {

            $content = explode( ' ', $input_content, $limit );

        } else {

            $content = explode( ' ', get_the_content(), $limit );

        }

        if ( count($content) >= $limit ) {

            array_pop( $content );
            $content = implode( " ", $content ) . '...';

        } else {

            $content = implode(" ",$content);

        }

        $content = preg_replace( '/\[.+\]/', '', $content );
        $content = apply_filters( 'the_content', $content );
        $content = str_replace( ']]>', ']]&gt;', $content );
        $content = strip_tags( $content, '' );

        if( $readmore == true ){

            $content .= ' <a href="' . get_permalink() . '" class="read-more">' . __( 'Continued', 'joldnl-soil' ) . '</a>';

        }

        return $content;

    }


}
