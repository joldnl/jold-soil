<?php

namespace Soil\Cli;

use Soil\Core\Functions;
use Soil\Core\ImageOptimization;

/**
 * Soil CLI Interface for checking and generating optimized upload cache
 */
class Assets {

    public function __construct()
    {
        if ( defined( 'WP_CLI' ) && WP_CLI ) {

            // Register main 'wp soil assets' command
            \WP_CLI::add_command( 'soil assets', [$this, 'index'] );

        }

    }


    /**
     * CLI Main index function
     * @param  [type] $args       [description]
     * @param  [type] $assoc_args [description]
     * @return [type]             [description]
     */
    public function index( $args, $assoc_args ) {

        if (isset($args) && !empty($args[0])) {

            // Register the 'wp soil assets optimize' command
            if ( $args[0] == 'optimize' ) {
                $this->optimize();
            }

            // Register the 'wp soil assets check' command
            elseif ( $args[0] == 'check' ) {
                $this->check();
            }

            // Register the 'wp soil assets optimize-needed' command
            elseif ( $args[0] == 'optimize-needed' ) {
                $this->optimizationNeeded();
            }

            // Register the 'wp soil assets list' command
            elseif ( $args[0] == 'list' ) {

                if ( isset($assoc_args) && !empty($assoc_args['raw']) ) {
                    $this->list( true );
                } else {
                    $this->list();
                }

            }

            else {
                \WP_CLI::log( '🚫  Command "' . $args[0] . '" not found!' );
            }

        } else {

            \WP_CLI::log( 'Soil assets cli tools usage:' );
            \WP_CLI::log( '$ wp soil assets check         Checks if opimization is needed' );
            \WP_CLI::log( '$ wp soil assets list          List all uploads' );
            \WP_CLI::log( '$ wp soil assets optimize      Optimize all uploads that are not optimized yet' );

        }

    }


    /**
     * Initiate the optimization process
     */
    public function optimize() {
        $optim = new ImageOptimization;
        $optim->optimizeAssets();
    }


    /**
     * Check if assets generation is needed
     * @return boolean  returns true not all assets are optimized
     */
    public function check() {

        $optim      = new ImageOptimization;
        $details    = $optim->assetsSummary();
        $jpgs       = ($details['assetsTotal'] - $details['assetsJpg']);
        $webps      = ($details['assetsTotal'] - $details['assetsWebp']);

        if ($optim->checkOptionNeedsOptimization() === 'no') {
            \WP_CLI::log( '😎  All assets (' . $details['assetsTotal'] . ') optimized! :)' );
        } else {
            \WP_CLI::log( '⚠️  Some assets need to be optimized' );
            \WP_CLI::log( '   ' . $jpgs . ' jpgs and ' . $webps . ' webp files need to be generated' );
        }

    }


    /**
     * Check if assets generation is needed
     * @return boolean  returns true not all assets are optimized
     */
    private function optimizationNeeded() {

        $optim  = new ImageOptimization;
        $status = $optim->checkOptionNeedsOptimization();

        if ($status == false || $status == 'no') {
            $status = 'no';
        }

        \WP_CLI::log( 'Optimization needed: ' . $status );

    }


    /**
     * Display a table with all original assets and the optimized status of the jpg and webp
     * @param  boolean  $raw    Option to return raw json data
     * @return mixed            Rerurns the table with results or the raw json encoded data
     */
    public function list( $raw = false ) {

        $optim      = new ImageOptimization;
        $files      = $optim->assetsDetails();
        $details    = $optim->assetsSummary();

        if ( $raw ) {

            \WP_CLI::log( json_encode($files) );

        } else {

            $tableHeader = '';
            $tableHeader .= "+--------------+-----------------------------------------------------------------+--------+--------+\n";
            $tableHeader .= "| Cache ID     | Original File                                                   | Jpg:   | WebP:  |\n";
            $tableHeader .= "+--------------+-----------------------------------------------------------------+--------+--------+";

            \WP_CLI::log( $tableHeader );

            foreach ($files as $file) {

                $statusOptimized    = ($file['optinizedJpg'] ? " ✅ " . $file['sizeDetails']['jpg']['percentage'] : " 🚫  - ");
                $statusWebp         = ($file['optinizedWebp'] ? " ✅ " . $file['sizeDetails']['webp']['percentage'] : " 🚫  - ");

                \WP_CLI::log( '| ' . Functions::fixedString($file['cacheId'], 12) . ' | ' . Functions::fixedString($file['filename'], 60) . '    |' . $statusOptimized . ' |' . $statusWebp . ' |');

                // print_r( $file['sizeDetails'] );
            }

            $tableFooter  = "+--------------+-----------------------------------------------------------------+--------+--------+\n";
            $tableFooter .= "|              | " . Functions::fixedString('Total asset files: ' . $details['assetsTotal'], 63) . " | " . Functions::fixedString($details['assetsJpg'] . '/' . $details['assetsTotal'], 6) . " | " . Functions::fixedString($details['assetsWebp'] . '/' . $details['assetsTotal'], 6) . " |\n";
            $tableFooter .= "+--------------+-----------------------------------------------------------------+--------+--------+";

            \WP_CLI::log( $tableFooter );

        }

    }

}
