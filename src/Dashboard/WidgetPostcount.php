<?php

/**
 *
 * Post counts widget functions (at a glance)
 *
 * Add all public post types and count to the 'At a glance' admin dashboard widget
 *
 * @package      Soil
 * @subpackage   Dashboard
 * @category     WidgetPostcount
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Dashboard;

use Soil\Core\User;

/**
 * Post counts widget functions
 */
class WidgetPostcount {



    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/27
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        // Add actions
        add_action( 'dashboard_glance_items',   array( $this , 'widget_items' ) ); // Add custom "At a glance" widget
        add_action( 'rightnow_end',             array( $this , 'widget_css' ) ); // Add custom "At a glance" css
        add_action( 'admin_head',               array( $this , 'widget_css' ) ); // Add custom "At a glance" css

    }




    /**
     *
     * widget_items
     *
     * Manipulate the default 'At a glance' dashboard widget to show all public post-types.
     *
     * @type	function
     * @date	2017/02/27
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function widget_items() {

        $post_types_excludes    = array();
        $post_types             = get_post_types(
            array(
                "public"    => true,
                '_builtin'  => false
            )
        );

        foreach ( $post_types as $post_type ) :

            $post_type_object   = get_post_type_object( $post_type );               // Get the post type object.
            $post_type_all      = wp_count_posts( $post_type );                     // Get tot total number of posts of this post type.
            $post_type_publish  = number_format_i18n( $post_type_all->publish );    // Get only the published posts of the posttype.

            $text               = _n( $post_type_object->labels->singular_name, $post_type_object->labels->name, intval( $post_type_publish ) ); // singular/plural text label for CPT

            echo '
            <li class="page-count custom-count ' . $post_type_object->name . '-count ">
                <i class="dashicons ' . $post_type_object->menu_icon . '"></i>
                <a href="edit.php?post_type=' . $post_type . '">' . $post_type_publish . ' ' . $text . '</a>
            </li>';

        endforeach;

    }



    /**
     *
     * widget_css
     *
     * Add some custom css for the widget
     *
     * @type	function
     * @date	2017/02/27
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @TODO    Move css to seperate css file.
     *
     * @return  n/a
     *
     */
    public function widget_css() {
        echo '
        <style>

            #dashboard_right_now .comment-count,
            #dashboard_right_now .post-count {
                display: none;
            }

            #dashboard_right_now .custom-count a:before {
                display: none;
            }

            #dashboard_right_now .custom-count .dashicons {
                display: inline-block;
                margin-right: 2px;

            }

            #dashboard_right_now .custom-count .dashicons {
                display: inline-block;
                margin-right: 2px;
                color: #888;
                font-size: 18px;
            }
            #dashboard_right_now .main {padding-bottom: 0px;}
            #wp-version-message {display: none;}
        </style>';
    }



}
