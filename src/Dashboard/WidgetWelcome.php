<?php
/**
 *
 * Customized welcome widget
 *
 * Improve and change the big welcome admin dashboard widget with custom content, links and text.
 *
 * @package      Soil
 * @subpackage   Dashboard
 * @category     WidgetWecome
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Dashboard;

use Soil\Core;
use Soil\Core\Functions;

/**
 * Welcome dashboard widget
 */
class WidgetWelcome {



    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/27
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        add_action( 'wp_dashboard_setup',       array( $this , 'welcome_panel' ) ); // Load custom welcome message in the dashboard
        add_action( 'admin_head',               array( $this , 'welcome_panel_cleanup' ) ); // Cleanup custom welcome message in the dashboard

    }



    /**
     *
     * welcome_panel
     *
     * Empty the default welcome panel and initialise custom content
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function welcome_panel() {

        // Remove the default welcome panel
        remove_action( 'welcome_panel', 'wp_welcome_panel' );

        // Add the new custom welcome panel content
        add_action( 'welcome_panel', array( $this, 'welcome_panel_content' ) );

        // Get the current user id
        $user_id = get_current_user_id();

        // Always load the welcome panel for non administrator users
        if ( Functions::get_user_roles_by_id( $user_id ) != 'administrator' ) {

            if ( get_user_meta( $user_id, 'show_welcome_panel', true ) != '1' ) {
                update_user_meta( $user_id, 'show_welcome_panel', 1 );
            }

        }

    }



    /**
     *
     * welcome_panel_content
     *
     * Load custom welcome panel content part
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function welcome_panel_content() {

        include_once( Core::plugin_path() . 'views/WidgetWelcome.php' );

    }



    /**
     *
     * welcome_panel_cleanup
     *
     * Add some cleanup css to the welcome panel
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function welcome_panel_cleanup() {
        echo '
        <style type="text/css">
            .welcome-panel-close {
                display: none;
            }
        </style>';
    }


}
