<?php

/**
 *
 * Frontend cleanup
 *
 * Cleanup frontend stuff, remove head tags, and clean several frontend related things.
 *
 * @package      Soil
 * @subpackage   Cleanup
 * @category     Admin Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Cleanup;


/**
 * Cleanup Class functions for the wp-admin
 */
class Frontend {


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/27
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        // Add filters
        add_filter( 'body_class',            array( $this, 'bem_body_classes' ) );          // Add BEM classes to the bodyclass
        add_filter( 'the_content',           array( $this, 'rel_external' ) );              // Replace target="_blank" with rel="external"
        add_filter( 'wp_nav_menu_items',     array( $this, 'rel_external' ) );              // Replace target="_blank" with rel="external"
        add_filter( 'request',               array( $this, 'search_request_filter' ) );     // Fix empty search string redirect
        // add_filter( 'embed_oembed_html',     array( $this, 'remove_head_oembed'), 10, 4 );  // Rmove oembed scripts
        add_filter( 'get_search_form',       array( $this, 'search_get_form' ) );           // Load searchform from the snippets folder
        add_filter( 'request',               array( $this, 'search_request_filter' ) );     // Fix empty search string redirect

        // Add actions
        add_action( 'init',                  array( $this, 'head_cleanup' ) );              // Clean up the head section
        add_action( 'template_redirect',     array( $this, 'search_nice_redirect' ) );      // Clean up search URL
		add_action( 'wp_footer',             array( $this, 'remove_scripts_footer' ) );		// Remove scripts inclusion in the wp-Footer
        add_action( 'after_setup_theme',     array( $this, 'remove_head_rest_api' ) );      // Remove the wp rest api meta tags from head
        add_action( 'after_setup_theme',     array( $this, 'remove_head_oembed' ) );        // Remove oembed meta tag from head
        add_action( 'init',                  array( $this, 'remove_head_rss' ) );           // Remove rss meta tags from head

    }



    /**
     *
     * head_cleanup
     *
     * Cleanup the frontend head section.
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function head_cleanup() {

        remove_action( 'wp_head', 'feed_links', 2);
        remove_action( 'wp_head', 'feed_links_extra', 3);
        remove_action( 'wp_head', 'rsd_link');
        remove_action( 'wp_head', 'wlwmanifest_link');
        remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
        remove_action( 'wp_head', 'wp_generator');

        remove_action( 'admin_print_styles', 'print_emoji_styles' );
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );


    }



    /**
     *
     * bem_body_classes
     *
     * Add custom body classes matching with BEM class naming principles.
     *
     * @see     https://codex.wordpress.org/Function_Reference/body_class
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   array    $classes    The current default WordPress body classes
     *
     * @return  array                The body classes array with added bem classes
     *
     */
    public function bem_body_classes( $classes ) {

        $bem_classes = array();

        // Add single class for pages
        if( get_post_type() && is_singular() && get_post_type() == 'page' ) {

            $bem_classes[] = 'body--page';

        // Add single class for all other than pages
        } elseif ( get_post_type() && is_singular() && get_post_type() != 'page' ) {

            $bem_classes[] = 'body--single';
            $bem_classes[] = 'body--single--' . get_post_type();

        // Add archive class
        } elseif( get_post_type() && is_post_type_archive( get_post_type() ) ) {

            $bem_classes[] = 'body--archive';
            $bem_classes[] = 'body--archive--' . get_post_type();

        }

        // Add post/page slug
        if (is_single() || is_page() && !is_front_page()) {
            $bem_classes[] = 'body--' . basename( get_permalink() );
        }

        // Add homepage class
        if( is_front_page() ) {
            $bem_classes[] = 'body--home';
        }

        // Add 404 class
        if( is_404() ) {
            $bem_classes[] = 'body--404';
        }

        // Add 404 class
        if( is_search() ) {
            $bem_classes[] = 'body--search';
        }

        // Add 404 class
        if( is_user_logged_in() ) {
            $bem_classes[] = 'body--loggedin';
        }

        $all_classes = array_merge( $classes, $bem_classes );
        $bem_classes = apply_filters( 'bem_body_classes', $all_classes );

        return $all_classes;


    }



    /**
     *
     * rel_external
     *
     * Replace target="_blank" with rel="external" for content and nav items
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   strimg    $content    The post or menu item content
     *
     * @return  string                The content with target=_blank replaced by rel=external
     *
     */
    public function rel_external( $content ) {

        $regexp = '/\<a[^\>]*(target="_([\w]*)")[^\>]*\>[^\<]*\<\/a>/smU';

        if( preg_match_all( $regexp, $content, $matches ) ){

            for ( $m = 0; $m < count( $matches[0] ); $m++ ) {

                if ( $matches[2][$m] == 'blank') {

                    $temp    = str_replace( $matches[1][$m], 'rel="external"', $matches[0][$m] );
                    $content = str_replace( $matches[0][$m], $temp, $content );

                } else if ($matches[2][$m] == 'self') {

                    $temp    = str_replace( ' ' . $matches[1][$m], '', $matches[0][$m] );
                    $content = str_replace( $matches[0][$m], $temp, $content );

                }

            }

        }

        return $content;

    }



    /**
     *
     * search_nice_redirect
     *
     * Redirects search results from /?s=query to /search/query/, converts %20 to +
     *
     * @see     http://txfx.net/wordpress-plugins/nice-search/
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function search_nice_redirect() {

        global $wp_rewrite;

        if ( !isset( $wp_rewrite ) || !is_object( $wp_rewrite ) || !$wp_rewrite->using_permalinks() ) {
            return;
        }

        $search_base = $wp_rewrite->search_base;

        if ( is_search() && !is_admin() && strpos( $_SERVER['REQUEST_URI'], "/{$search_base}/" ) === false ) {

            wp_redirect( home_url( "/{$search_base}/" . urlencode( get_query_var('s') ) ) );

            exit();

        }

    }



    /**
     *
     * search_request_filter
     *
     * Fix empty search queries and redirect them to the home page
     *
     * @link    http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage#post-1772565
     * @link    http://core.trac.wordpress.org/ticket/11330
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   array      $query_vars     The current query var.
     *
     * @return  array                      The manipulated query var with empty s parameter.
     *
     */
    public function search_request_filter( $query_vars ) {

        if ( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {

            $query_vars['s'] = ' ';

        }

        return $query_vars;

    }



    /**
     *
     * remove_scripts_footer
     *
     * Remove script inclusion in wp_footer by WordPress
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function remove_scripts_footer(){

        wp_deregister_script( 'wp-embed' );

    }



    /**
     *
     * remove_head_rest_api
     *
     * Remove the wp rest api meta tags from wp_head
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function remove_head_rest_api() {

        remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
        remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

    }



	/**
     *
     * remove_head_oembed
     *
     * Remove oembed meta tag from head
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function remove_head_oembed() {

        remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

    }



	/**
	 *
	 * remove_head_rss
	 *
	 * Remove rss meta tags from head
	 *
	 * @type	function
	 * @date	2017/02/26
	 * @since	0.1.0
	 * @author  Jurgen Oldenburg
	 *
	 * @return  n/a
	 *
	 */
    public function remove_head_rss() {

        remove_action( 'wp_head', 'feed_links_extra', 3 );
        remove_action( 'wp_head', 'feed_links', 2 );

    }



}
