<?php

/**
 *
 * Cleanup yoast seo
 *
 * Clean up several diefrent yoast seo stuff
 *
 * @package      Soil
 * @subpackage   Cleanup
 * @category     Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Cleanup;


/**
 * Cleanup yoast seo Class
 */
class Yoast {


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/19
     * @since	2.0.0-alpha
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        // Add filters
        add_filter( 'wpseo_metabox_prio',   array( $this, 'yoast_to_bottom' ) );

        // Add actions
        add_action( 'wp',                   array( $this, 'clean_columns'), 999 );
        add_action( 'wp_dashboard_setup',   array( $this, 'dashboard_overview_widget' ) ); // Clean up overview dashboard widget
        add_action( 'admin_head',           array( $this, 'hide_sidebar_ads' ) );
        add_action( 'admin_init',           array( $this, 'ignore_tour'), 999 );
        add_action( 'admin_bar_menu',       array( $this, 'remove_adminbar'), 999 );
        add_action( 'wp_head',              array( $this, 'rel_canonical') );


    }


    /**
     *
     * yoast_to_bottom
     *
     * Move yoast menu item to the bottom of the ammin menu
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  string  Set menu item priority to low
     *
     */
    function yoast_to_bottom() {

        return 'low';

    }



    /**
     *
     * clean_columns
     *
     * Cleanup dashboard columns from the Yoast SEO plugin
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function clean_columns() {

        // Check of this is not disabled trough settings
        if ( get_option('soil_yoast_clean_columns') == true ) {

            $post_types = get_post_types( array( 'public'   => true ) );

            if( $post_types ) {

                foreach( $post_types as $post_type ) {

                    add_filter( 'manage_edit-' . $post_type . '_columns', [$this,'remove_columns'] , 10, 1 );

                }

            }
        }

    }



    /**
     *
     * remove_columns
     *
     * Remove the WP SEO columns
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   array   $columns    The array with all current columns
     * @return  array               The array with wpso columns removed
     *
     */
    public function remove_columns( $columns ) {

        // Unset WP SEO columns
        unset( $columns['wpseo-title'] );
        unset( $columns['wpseo-metadesc'] );
        unset( $columns['wpseo-focuskw'] );
        unset( $columns['wpseo-score'] );
        unset( $columns['wpseo-score-readability'] );
        unset( $columns['wpseo-links'] );


        return $columns;

    }



    /**
     *
     * dashboard_overview_widget
     *
     * Cleanup the Yoast SEO dashboard overview widget
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function dashboard_overview_widget() {

        // Check of this is not disabled trough settings
        if ( get_option('soil_yoast_hide_dashboard_widget') == true ) {

            remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );

        }

        return false;

    }



    /**
     *
     * hide_sidebar_ads
     *
     * Hide the yoast seo sidbar ads
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  string      Inline css styles
     *
     */
    public function hide_sidebar_ads() {

        // Check of this is not disabled trough settings
        if ( get_option('soil_yoast_hide_sidebar_ads') == true ) {

            echo '<style type="text/css">#wpseo-dismiss-about, #sidebar-container.wpseo_content_cell, .wpseotab.active > p:nth-child(6), .wpseotab.active > p:nth-child(7), #wpseo-dismiss-gsc {display:none;}</style>';

        }

        return false;

    }



    /**
     *
     * ignore_tour
     *
     * Disable the intro tour after activating Yoast SEO plugin for the first time
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function ignore_tour() {

        // Check of this is not disabled trough settings
        if ( get_option('soil_yoast_ignore_tour') == true ) {

            update_user_meta( get_current_user_id(), 'wpseo_ignore_tour', true );
        }

        return false;

    }



    /**
     *
     * remove_adminbar
     *
     * Remove all Yoast SEO menu items from the admin bar
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function remove_adminbar() {

        // Check of this is not disabled trough settings
        if ( get_option('soil_yoast_remove_adminbar') == true ) {

            global $wp_admin_bar;

            // remove the entire menu
            $wp_admin_bar->remove_node( 'wpseo-menu' );

            // remove WordPress SEO Settings
            // $wp_admin_bar->remove_node( 'wpseo-settings' );

            // remove keyword research information
            //$wp_admin_bar->remove_node( 'wpseo-kwresearch' );

        }

    }



    /**
     *
     * rel_canonical
     *
     * Replace the Yoast canonical rel link element with a clean version.
     *
     * @type	function
     * @date	2017/02/27
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  string      The custom cannonical url link element
     *
     */
    public function rel_canonical() {

        global $wp_the_query;

        if ( !class_exists('WPSEO_Frontend') ) {

            remove_action( 'wp_head', 'rel_canonical' );

            if ( !is_singular() ) {
                return;
            }

            if ( !$id = $wp_the_query->get_queried_object_id() ) {
                return;
            }

            $link = get_permalink($id);

            echo "\t<link rel=\"canonical\" href=\"$link\">\n";


        }

    }

}
