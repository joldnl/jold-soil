<?php

/**
 *
 * Cleanup yoast head comments seo
 *
 * Clean up het html comments from the yoast section in the <head>
 *
 * @package      Soil
 * @subpackage   Cleanup
 * @category     Cleanup
 * @author       Jurgen Oldenburg
 *
 * Original plugin:
 * https://wordpress.org/plugins/remove-yoast-seo-comments/
 *
 */


namespace Soil\Cleanup;


/**
 * Cleanup yoast seo Class
 */
class YoastHead {

    /**
     * version
     * @var mixed
     */
    private $version = '3.1';

    /**
     * debug_marker_removed
     * @var boolean
     */
	private $debug_marker_removed = false;

    /**
     * head_marker_removed
     * @var boolean
     */
	private $head_marker_removed = false;

    /**
     * backup_plan_active
     * @var boolean
     */
	private $backup_plan_active = false;



    /**
     * __construct
     */
	public function __construct() {

		add_action( 'init',       [ $this, 'bundle' ], 1);

	}


    /**
     * Check yoast-seo version and handle acordingly
     * @return na
     */
	public function bundle() {


		if( defined( 'WPSEO_VERSION' ) ) {

			$debug_marker = ( version_compare( WPSEO_VERSION, '4.4', '>=' ) ) ? 'debug_mark' : 'debug_marker';

			// Main function to unhook the debug msg
			if( class_exists( 'WPSEO_Frontend' ) && method_exists( 'WPSEO_Frontend', $debug_marker ) ) {

				remove_action( 'wpseo_head',        array( \WPSEO_Frontend::get_instance(), $debug_marker ) , 2);

				$this->debug_marker_removed = true;

				// also removes the end debug msg as of yoast-seo 5.9
				if( version_compare( WPSEO_VERSION, '5.9', '>=' ) ) {
                    $this->head_marker_removed = true;
                }

			}

			// Compatible solution for everything below yoast-seo 5.8
			if( class_exists( 'WPSEO_Frontend' ) && method_exists( 'WPSEO_Frontend', 'head' ) && version_compare( WPSEO_VERSION, '5.8', '<' ) ) {

				remove_action( 'wp_head',           array( WPSEO_Frontend::get_instance(), 'head' ) , 1 );
				add_action( 'wp_head',              array( $this, 'rewrite'), 1 );

				$this->head_marker_removed = true;

			}

			// Temporary solution for all installations on yoast-seo 5.8
			if( version_compare( WPSEO_VERSION, '5.8', '==' ) ) {

				add_action( 'get_header',           array( $this, 'buffer_header' ) );
				add_action( 'wp_head',              array( $this, 'buffer_head' ), 999 );

				$this->head_marker_removed = true;

			}

			// Backup solution
			if( $this->operating_status() == 2 ) {

				add_action( 'get_header',           array( $this, 'buffer_header' ) );
				add_action( 'wp_head',              array( $this, 'buffer_head' ), 999 );

			}

			if( current_user_can('manage_options') ) {

                add_action( 'wp_dashboard_setup',   array( $this, 'dash_widget' ) );

            }

		}

	}


    /**
     * Return the operation status number
     * @return  int  Status number (1,2 or 3)
     */
	public function operating_status() {

        // Operation status finished
		if( $this->debug_marker_removed && $this->head_marker_removed ) {
			return 1;
		}
        // Operation status not-finished
        elseif( !$this->debug_marker_removed && $this->head_marker_removed || $this->debug_marker_removed && !$this->head_marker_removed ) {
			return 2;
		}
        // Operation status failed
        else {
			return 3;
		}

	}


    /**
     * Register the dashboard widget
     * @return na
     */
	public function dash_widget() {
		// wp_add_dashboard_widget( 'dashboard_widget', 'Remove Yoast SEO Comments', array( $this, 'dash_widget_content' ) );
	}



    /**
     * Create the dashboard widget content
     * @return mixed    The widget content html
     */
	public function dash_widget_content() {

		if( $this->operating_status() == 1 ) {

			$status  = '<span style="color:#04B404;font-weight:bold">Fully supported</span>';
			$content = '<p>Version ' . WPSEO_VERSION . ' of Yoast SEO is fully supported by RYSC ' . $this->version . '. The HTML comments have been removed from your front-end source code.</p>';

		} elseif( $this->operating_status() == 2 ) {

			$status  = '<span style="color:#FF8000;font-weight:bold">Partially supported</span>';
			$content = '<p>Version ' . WPSEO_VERSION . ' of Yoast SEO is not properly supported by RYSC ' . $this->version . '. Some functions are not working. A backup solution has been enabled to keep the HTML comments removed.</p>';

		} elseif( $this->operating_status() == 3 ) {

			$status  = '<span style="color:#DF0101;font-weight:bold">Not supported</span>';
			$content = '<p>Version ' . WPSEO_VERSION . ' of Yoast SEO is not supported by RYSC ' . $this->version . '. A backup solution has been enabled to keep the HTML comments removed.</p>';

		}

		echo '<div class="activity-block"><h3><span class="dashicons dashicons-admin-plugins"></span> Yoast SEO ' . WPSEO_VERSION . ' Compatibility Status: ' . $status . '</h3></div>';
		echo $content;

	}



    /**
     * Compatible filtering solution for everything below yoast-seo 5.8
     * @return [type] [description]
     */
	public function rewrite() {

		$rewrite      = new ReflectionMethod( 'WPSEO_Frontend', 'head' );

		$filename     = $rewrite->getFileName();
		$start_line   = $rewrite->getStartLine();
		$end_line     = $rewrite->getEndLine()-1;

		$length       = $end_line - $start_line;
		$source       = file( $filename );
		$body         = implode( '', array_slice($source, $start_line, $length) );
		$body         = preg_replace( '/echo \'\<\!(.*?)\n/', '', $body );

		eval( $body );

	}



	// temp solution for all yoast-seo installations on 5.8, and also the backup solution in the future
	public function buffer_header() {

		ob_start(function ($o) {
			return preg_replace('/\n?<.*?yoast.*?>/mi','',$o);
		});

	}



	public function buffer_head() {

		ob_end_flush();

	}


}
