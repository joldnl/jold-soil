<?php

/**
 *
 * Cleanup functions for the wp-admin
 *
 * Main wp-admin cleanup functions enhancing the default wp-admin.
 *
 * @package      Soil
 * @subpackage   Cleanup
 * @category     Admin Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Cleanup;

use Soil\Core;
use Soil\Core\User;
use Soil\Core\Vendor;


/**
 * Cleanup Class functions for the wp-admin
 */
class Admin {


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {


        // Add actions
        add_action( 'admin_init',               array( $this, 'core_updates_check' ) );                         // Disable the admin bar for logged in users.
        add_action( 'wp_loaded',                array( $this, 'disable_admin_bar' ) );                          // Disable the admin bar for logged in users.
        add_action( 'admin_head',               array( $this, 'disable_admin_bar_setting' ) );                  // Remove the admin bar setting from the profile page
        add_action( 'admin_menu',               array( $this, 'remove_admin_menus' ) );                         // Remove some default admin menu items
        add_action( 'wp_dashboard_setup',       array( $this, 'remove_dashboard_widgets' ) );                   // Remove some default admin dashboard widgets
        add_action( 'widgets_init',             array( $this, 'remove_widgets' ) );                             // Remove all default WordPress theme widgets
        add_action( 'admin_menu',               array( $this, 'remove_editscreen_widgets' ) );                  // Hide slug metabox on edit post and page screens

        add_action( 'admin_bar_menu',           array( $this, 'remove_adminbar_nodes'), 999 );

        // Add filters
        add_filter( 'admin_footer_text',        array( $this, 'admin_footer' ) );                               // Set the wp-admin footer contents to custom text and url.
        add_filter( 'admin_head',               array( $this, 'admin_footer_version' ) );                               // Set the wp-admin footer contents to custom text and url.

        add_filter( 'image_size_names_choose',  array( $this, 'get_all_image_sizes' ) );                        // Add all custom image sizes to the "Size" select box in the media pop up
        // add_filter( 'hidden_meta_boxes',        array( $this, 'hide_page_attributes_metabox' ), 10, 2 );     // Hide page attributes metabox on edit page screens

        // Force One column admin dashboard
        add_action( 'admin_head',               array( $this, 'dashboard_remove_postboxes' ) );                 // Cleanup custom welcome message in the dashboard
        add_filter( 'screen_layout_columns',    array( $this, 'dashboard_columns' ) );                          // Force 1 column dashboard layout
        add_filter( 'get_user_option_screen_layout_dashboard', array( $this , 'dashboard_layout_columns' ) );   // Force 1 column dashboard layout


        // Add actions
        // add_action( 'wp_dashboard_setup',       array( $this, 'dashboard_tidy' ) );                          // Clean up dashboard widgets


    }



    /**
     *
     * disable_admin_bar
     *
     * Disable the admin bar on the frontend of the site
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function disable_admin_bar() {

        if ( get_option('soil_hide_adminbar') == 'true' ) {

            show_admin_bar( false );

        }

    }



    /**
     *
     * disable_admin_bar_setting
     *
     * Hide the admin bar setting form the profile page.
     *
     * @type    function
     * @date    2017/02/26
     * @since   1.0.13
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function disable_admin_bar_setting() {

        if ( get_option('soil_hide_adminbar') == 'true' ) {

            echo '<style media="screen">tr.user-admin-bar-front-wrap { display: none; }</style>';

        }

    }



    /**
     *
     * remove_admin_menus
     *
     * Remove admin menu items
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function remove_admin_menus () {

        global $menu;

        $restricted = [
            __( 'Links' )
        ];

        if ( get_option('soil_hide_posts') == 'true' ) {
            $restricted[] = __( 'Posts' );
        }

        if ( get_option('soil_hide_comments') == 'true' ) {
            $restricted[] = __( 'Comments' );
        }

        if ( get_option('soil_hide_tools') == 'true' ) {
            if( User::is_client_role() ) {
                $restricted[] = __( 'Tools' );
            }
        }

        if ( get_option('soil_hide_media') == 'true' ) {
            if( User::is_client_user() ) {
                $restricted[] = __( 'Media' );
            }
        }


        end ( $menu );

        while ( prev( $menu ) ) {

            $value = explode( ' ', $menu[ key( $menu ) ][0] );

            if ( in_array( $value[0] != NULL ? $value[0] : '' , $restricted ) ) {

                unset( $menu[ key( $menu ) ] );

            }

        }

    }



    /**
     *
     * remove_dashboard_widgets
     *
     * Hide some admin dashboard widgets
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function remove_dashboard_widgets() {

        remove_meta_box( 'dashboard_quick_press',       'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments',   'dashboard', 'normal' );
        remove_meta_box( 'dashboard_incoming_links',    'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins',           'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary',           'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary',         'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts',     'dashboard', 'side' );
        remove_meta_box( 'dashboard_activity',          'dashboard', 'side' );
        // remove_meta_box( 'dashboard_right_now',         'dashboard', 'normal' );

    }



    /**
     *
     * remove_edit_screen_widgets
     *
     * Hide slug metabox on edit post and page screens
     *
     * @type    function
     * @date    2017/03/02
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @TODO:    Make this working for all custom post types, and pages.
     *
     * @return  n/a
     *
     */
    public function remove_editscreen_widgets() {

        remove_meta_box( 'slugdiv', 'post', 'side' );
        remove_meta_box( 'slugdiv', 'page', 'side' );

    }



    /**
     *
     * remove_widgets
     *
     * Remove all default WordPress theme widgets
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function remove_widgets() {

        unregister_widget( 'WP_Widget_Pages' );
        unregister_widget( 'WP_Widget_Calendar' );
        unregister_widget( 'WP_Widget_Archives' );
        unregister_widget( 'WP_Widget_Links' );
        unregister_widget( 'WP_Widget_Meta' );
        unregister_widget( 'WP_Widget_Search' );
        unregister_widget( 'WP_Widget_Text' );
        unregister_widget( 'WP_Widget_Categories' );
        unregister_widget( 'WP_Widget_Recent_Posts' );
        unregister_widget( 'WP_Widget_Recent_Comments' );
        unregister_widget( 'WP_Widget_RSS' );
        unregister_widget( 'WP_Widget_Tag_Cloud' );
        unregister_widget( 'WP_Nav_Menu_Widget' );

    }



    /**
     *
     * change_admin_footer
     *
     * Set the wp-admin footer contents to custom text and url.
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  string      New admin footer text
     *
     */
    public function admin_footer() {

        $vendor = Core::vendor();

        // Check if footer text is disabled
        $disabled = get_option('soil_admin_footer_disable');

        // Get the custom footer html
        $custom   = get_option('soil_admin_footer_html');


        // Check if footer text is disabled
        if ( $disabled == true ) {

            echo '';

        } else {

            // Check if there is custom footer html
            if ( !empty($custom) ) {
                echo $custom;
            }

            // Else echo the default html.
            else {
                echo '<p><a href="' . $vendor->websiteUrl . '" target="_blank">' . $vendor->name . ' CMS</a></p>';
            }

        }

    }



    /**
     * Remove the WordPress version from the admin footer
     * @return  string      Css to hide the version
     */
    public function admin_footer_version() {

        echo '
            <style>
                #footer-upgrade {display: none;}
            </style>
        ';

    }



    /**
     *
     * get_all_image_sizes
     *
     * Add all custom image sizes to the "Size" select box in the media pop up
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   array    $sizes     The standard array of image sizes
     *
     * @return  string              Image sizes array with all custom images sizes
     *
     */
    public function get_all_image_sizes( $sizes ) {

        $image_sizes         = get_intermediate_image_sizes();
        $image_sizes_all     = array();

        // Loop through all images sizes
        if( $image_sizes ) {

            $counter = '0';

            foreach ($image_sizes as $image_size) {

                $counter++;
                $nice_image_size = ucfirst( str_replace( '-', ' ', $image_size ) );
                $image_sizes_all[$image_size] = $nice_image_size;

            }

        }

        return $image_sizes_all;
    }



    /**
     *
     * hide_page_attributes_metabox
     *
     * Hide page attributes metabox on edit page screens
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param      boolean     $hidden
     *
     * @return     string
     *
     */
    public function hide_page_attributes_metabox( $hidden ) {

        $hidden[] = 'pageparentdiv';

        return $hidden;

    }



    /**
     *
     * remove_core_updates
     *
     * Hide update messages
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return     object      The updates array
     *
     */
    public function core_updates_remove() {

        global $wp_version;
        return(object) array(
            'last_checked'      => time(),
            'version_checked'   => $wp_version,
            'updates'           => array()
        );

    }


    /**
     *
     * core_updates_check
     *
     * Check if user is not an administrator and run remove_core_updates filters.
     *
     * @type    function
     * @date    2017/03/10
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return     n/a
     *
     */
    public function core_updates_check() {

        // Only execute on wp-admin screens.
        if ( is_admin() ) {

            // Check if user is not an administrator
            if ( !User::is_admin() ) {

                add_filter('pre_site_transient_update_core',        array( $this, 'core_updates_remove' ) );
                add_filter('pre_site_transient_update_plugins',     array( $this, 'core_updates_remove' ) );
                add_filter('pre_site_transient_update_themes',      array( $this, 'core_updates_remove' ) );

            }

        }

    }




    /**
     *
     * dashboard_columns
     *
     * Force admin dashboard to one column
     *
     * @type    function
     * @date    2017/03/09
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   array    $columns    The dashboard columns array
     *
     * @return  array    $columns    The dashboard columns array with only one column
     *
     */
    public function dashboard_columns( $columns ) {

        if ( get_option('soil_dashboard_columns') == 'true' ) {

            $columns['dashboard'] = 1;

            return $columns;
        }

    }



    /**
     *
     * dashboard_layout_columns
     *
     * Force dashboard columns to one columnd
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  int    Return 1 for one column
     *
     */
    public function dashboard_layout_columns() {

        if ( get_option('soil_dashboard_columns') == 'true' ) {

            return 1;

        }

    }



    /**
     *
     * dashboard_remove_postboxes
     *
     * Hide unused dashboard metabox areas
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  string         Inline css code to hide the metabox areas
     *
     */
    public function dashboard_remove_postboxes() {

        if ( get_option('soil_dashboard_columns') == 'true' ) {

            echo '
            <style type="text/css">
                @media only screen and (max-width: 1800px) and (min-width: 1500px) {
                    body.wp-admin #wpbody-content #dashboard-widgets #postbox-container-1 {
                        width: 100% !important;
                    }
                }

                #dashboard-widgets-wrap #dashboard-widgets #postbox-container-2,
                #dashboard-widgets-wrap #dashboard-widgets #postbox-container-3,
                #dashboard-widgets-wrap #dashboard-widgets #postbox-container-4 {
                    display: none !important;
                }

            </style>';

        }

    }



    /**
     *
     * dashboard_tidy
     *
     * Clean up some dashboard widgets
     *
     * @type    function
     * @date    2017/02/26
     * @since   0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  string         Inline css code to hide the metabox areas
     *
     */
    public function dashboard_tidy() {

        global $wp_meta_boxes, $current_user;

        unset(
            // $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'],
            $wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity'],
            $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'],
            $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']
        );

    }





    /**
     *
     * remove_adminbar_nodes
     *
     * Remove items from the admin bar
     *
     * @type    function
     * @date    2017/02/26
     * @since   1.0.14
     * @author  Jurgen Oldenburg
     *
     * @param   object  $wp_admin_bar   The original admin bar object containing all items.
     *
     * @return  n/a
     *
     */
    public function remove_adminbar_nodes( $wp_admin_bar ) {

        $wp_admin_bar->remove_node( 'wp-logo' );

        if ( get_option('soil_hide_posts') == 'true' ) {
            $wp_admin_bar->remove_node( 'new-post' );
            // $wp_admin_bar->remove_node( 'new-content' );
        }

        if ( get_option('soil_hide_comments') == 'true' ) {
            $wp_admin_bar->remove_node( 'comments' );
        }

        if ( get_option('soil_hide_media') == 'true' ) {
            if( User::is_client_user() ) {
                $wp_admin_bar->remove_node( 'new-media' );
            }
        }

        if( User::is_client_user() ) {
            $wp_admin_bar->remove_node( 'new-user' );
        }

    }

}
