<?php

/**
 *
 * Cleanup emoji's
 *
 * Disable and cleanup all emoji related stuff
 *
 * @package      Soil
 * @subpackage   Core
 * @category     Cleanup
 * @author       Jurgen Oldenburg
 *
 */


namespace Soil\Cleanup;


/**
 * Emoji cleanup Class
 */
class Emoji {


    // public $MenuItemClass = 'Lumberjack\Core\MenuItem';
    // public $PostClass = 'Lumberjack\PostTypes\Post';


    /**
     *
     * __construct
     *
     * Main class costructor function
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    function __construct() {

        add_action( 'init',                 array( $this, 'disable_emoji' ) );

    }



    /**
     *
     * disable_emoji
     *
     * Dispable all empoji conversion trough out the admin and frontend
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @return  n/a
     *
     */
    public function disable_emoji() {

        // Remove actions doing stuff with emojis
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );

        // Remove filters doing stuff with emojis
        remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

        // Filter to remove TinyMCE emojis
        add_filter( 'tiny_mce_plugins', array( $this, 'disable_emoji_tinymce' ) );

    }



    /**
     *
     * disable_emoji_tinymce
     *
     * Dispable all empoji conversion in any TinyMce wysiwyg editor
     *
     * @type	function
     * @date	2017/02/26
     * @since	0.1.0
     * @author  Jurgen Oldenburg
     *
     * @param   $plugins    The edotitor plugins array
     *
     * @return  array       The manipulated plugins array, without the emoji plugin
     *
     */
    public function disable_emoji_tinymce( $plugins ) {

      if ( is_array( $plugins ) ) {

        return array_diff( $plugins, array( 'wpemoji' ) );

      } else {

        return array();

      }

    }



}
