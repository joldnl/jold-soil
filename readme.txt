=== Jold: Soil ===
Contributors: Jurgen Oldenburg
Donate link: https://www.jold.nl/
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Tags: Soil, Firestarter, Jold, WordPress, Core Plugin
Requires at least: 4.6
Tested up to: 4.9.8
Stable tag: 1.0

The main Groundwater core functionality plugin containing global settings, cleanup and WordPress modifications for a clean and better user experience.



== Changelog ==


= 1.2.6 =

Release Date: Nov 14th, 2018

* Add WPTRT Admin Notices and update composer depts
* Show admin notice from WPTRT Admin Notices
* Add vendor folder to git remove from gitignore
* Update messages, add link to permalinks and code cleanup
* Remove Rewrites, moved to Firestarter
* All of the functionality in this class was theme related, so this is moved to Firestarter as of now
* Remove Admin Notices class as this is not used anymore in Soil
* Hide default user roles for client roles (client_admin and client_user)
* Update user caps for client_admin user
* Add Gravity Forms caps for client_ user and admin
* Make client_roles and is_client_role functions static
* Update capabilities for client_user and client_admin
* Add 'hide tools from admin' setting and update settings descriptions


= 1.2.5 =

Release Date: Okt 27th, 2019

* Fix Windows compatible paths


= 1.2.4 =

Release Date: Nov 14th, 2018

* WebP images are only served if WebP is supported by the client
* Improve optimization report on settings page
* Code cleanup


= 1.2.3 =

Release Date: Nov 14th, 2018

* Improve cron and shortpixel invocation


= 1.2.2 =

Release Date: Nov 14th, 2018

* Improve cron and shortpixel invocation


= 1.2.1 =

Release Date: Nov 14th, 2018

* Implement reworked image optimization and add cli commands for optimizing and checking optimized images.


= 1.2 =

Release Date: Aug 11th, 2018

* Add ShortPixel Image Optimization which saves compressed and WebP versions of uploaded assets and serves them accordingly.


= 1.1 =

Release Date: Jul 28th, 2018

* Settings groups are grouped in tabs on one page!
* Moved the white label settings page into a new tab
* New Yoast SEO cleanup settings tab
* All new Yoast SEO header comments stripper


= 1.0.25 =

Release Date: Jul 27th, 2018

* Put back the welcome dashboard widget
* Add vendor details that display on welcome widget
* Clean out WordPress traces
* Add wp admin footer options (hide or manipulate)
* Add whitelabal CMS features and settings


= 1.0.24 =

Release Date: Dec 12th, 2017

* Update support email to info@jold.nl.
* Update contact details.
* Improve code formatting.
* Change footer link and text to Jold.


= 1.0.23 =

Release Date: Dec 12th, 2017

* Update bitbucket url's.
* Remove pms72 references.


= 1.0.22 =

Release Date: Dec 12th, 2017

* Update plugin version.


= 1.0.21 =

Release Date: Dec 6th, 2017

* Update composer file with correct details.


= 1.0.21 =

Release Date: Dec 6th, 2017

* Update 'tested up to' WordPress version to 4.8.1.
* Remove all PMS72 references.
* Update path replacement for windows filesystem.
* Make admin settings path compattible with windows.
* Disable welcome widget.
* Update plugin metadata details.


= 1.0.20 =

Release Date: Sept 20th, 2017

* Add missing 'original' default image size to wpthumb ignore list.


= 1.0.19 =

Release Date: Sept 17th, 2017

* Fix and allow oembeds in content fields.
* TODO: Make this (and more cleanup options) available as settings in the wp-admin.


= 1.0.18 =

Release Date: Sept 9th, 2017

* Add setting to include or exclude default soil client_admin and client_user roles.


= 1.0.17 =

Release Date: August 27th, 2017

* Fix image dimentions error in wp_thumb class



= 1.0.16 =

Release Date: August 20th, 2017

* Add 'user_has_role' method to check if a user object has a certain user_role.
* Add soil settings-page to admin menu ()



= 1.0.15 =

Release Date: April 19th, 2017

* Add this WordPress compatible readme file.



= 1.0.14 =

Release Date: April 19th, 2017

* Remove items from the admin bar (this can be changed in the new settings page).


= 1.0.13 =

Release Date: April 19th, 2017

* Add setting page with options to hide posts, media and comments



= 1.0.12 =

Release Date: April 6th, 2017

* Add .editorconfig
* Remove default medium image size
* Enable the admin bar by default. In future versions this will be a setting.



= 1.0.11 =

Release Date: April 4th, 2017

* Implement and use rewrite functions
